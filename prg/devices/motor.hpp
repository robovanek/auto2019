//
// Created by kuba on 18.3.19.
//

#ifndef PRG_MOTOR_HPP
#define PRG_MOTOR_HPP

#include "devices.hpp"
#include "../robofat/pid_param.h"

namespace rg {
    enum class motor_type {
        large, medium, unknown
    };

    enum motor_state {
        motor_none = 0x00,
        motor_running = 0x01,
        motor_ramping = 0x02,
        motor_holding = 0x04,
        motor_overloaded = 0x08,
        motor_stalled = 0x10
    };

    enum class stop_action {
        coast, brake, hold
    };

    enum class motor_action {
        run_forever,
        run_to_abs_pos,
        run_to_rel_pos,
        run_timed,
        run_direct,
        stop,
        reset
    };

    class motor {
    public:
        static constexpr const char *large_name = "EV3 Large Motor";
        static constexpr const char *large_driver = "lego-ev3-l-motor";

        static constexpr const char *medium_name = "EV3 Medium Motor";
        static constexpr const char *medium_driver = "lego-ev3-m-motor";

        explicit motor(device_ptr dev);

        std::string getAddress();

        motor_type getType();

        robofat::pid_consts<int> getHoldPID();

        void setHoldPID(const robofat::pid_consts<int> &values);

        robofat::pid_consts<int> getSpeedPID();

        void setSpeedPID(const robofat::pid_consts<int> &values);

        stop_action getStopAction();

        void setStopAction(stop_action action);

        motor_state getState();

        int getCurrentDutyCycle();

        int getCurrentSpeed();

        int getCurrentPosition();

        int getMaximumSpeed();

        bool isNormalPolarity();

        void setPolarity(bool normal);


        int getTimeSP();

        void setTimeSP(int value);

        int getSpeedSP();

        void setSpeedSP(int value);

        int getPositionSP();

        void setPositionSP(int value);

        int getRampUpSP();

        void setRampUpSP(int value);

        int getRampDownSP();

        void setRampDownSP(int value);

        int getDutyCycleSP();

        void setDutyCycleSP(int value);

        void doThe(motor_action action);

    protected:
        device_ptr dev;
        attribute_ptr a_address; // ok
        attribute_ptr a_command; // ok
        attribute_ptr a_driver_name; // ok
        attribute_ptr a_duty_cycle; // ok
        attribute_ptr a_duty_cycle_sp; // ok
        attribute_ptr a_hold_kp; // ok
        attribute_ptr a_hold_ki; // ok
        attribute_ptr a_hold_kd; // ok
        attribute_ptr a_max_speed; // ok
        attribute_ptr a_polarity; // ok
        attribute_ptr a_position; // ok
        attribute_ptr a_position_sp; // ok
        attribute_ptr a_ramp_up_sp; // ok
        attribute_ptr a_ramp_down_sp; // ok
        attribute_ptr a_speed; // ok
        attribute_ptr a_speed_kp; // ok
        attribute_ptr a_speed_ki; // ok
        attribute_ptr a_speed_kd; // ok
        attribute_ptr a_speed_sp; // ok
        attribute_ptr a_state; // ok
        attribute_ptr a_stop_action; // ok
        attribute_ptr a_time_sp; // ok
    };
}


#endif //PRG_MOTOR_HPP
