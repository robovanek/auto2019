//
// Created by kuba on 18.3.19.
//

#include <cstring>
#include <scheduler.hpp>
#include "lego_gyro.hpp"

rg::lego_gyro::lego_gyro(rg::device_ptr ptr) : sensor(std::move(ptr)) {}

void rg::lego_gyro::setMode(rg::gyro_mode mode) {
    std::string modeStr;
    switch (mode) {
        case gyro_mode::GYRO_ANG:
            modeStr = "GYRO-ANG";
            break;
        case gyro_mode::GYRO_RATE:
            modeStr = "GYRO-RATE";
            break;
        case gyro_mode::GYRO_FAS:
            modeStr = "GYRO-FAS";
            break;
        case gyro_mode::GYRO_GnA:
            modeStr = "GYRO-G&A";
            break;
        case gyro_mode::GYRO_CAL:
            modeStr = "GYRO-CAL";
            break;
    }
    setModeString(modeStr);
}

rg::gyro_mode rg::lego_gyro::getMode() {
    std::string modeStr = getModeString();
    if (modeStr == "GYRO-ANG") {
        return gyro_mode::GYRO_ANG;
    } else if (modeStr == "GYRO-RATE") {
        return gyro_mode::GYRO_RATE;
    } else if (modeStr == "GYRO-FAS") {
        return gyro_mode::GYRO_FAS;
    } else if (modeStr == "GYRO-G&A") {
        return gyro_mode::GYRO_GnA;
    } else if (modeStr == "GYRO-CAL") {
        return gyro_mode::GYRO_CAL;
    } else {
        throw std::runtime_error("unknown gyro sensor mode: " + modeStr);
    }
}

int16_t rg::lego_gyro::readAngle() {
    return readBinaryInteger<int16_t>(readBinaryData());
}

int16_t rg::lego_gyro::readRate() {
    return readBinaryInteger<int16_t>(readBinaryData());
}

std::tuple<int16_t, int16_t> rg::lego_gyro::readAngleAndRate() {
    auto &&tuple = readBinaryData();

    int16_t gyro, accel;
    if (std::get<1>(tuple) < 4) {
        throw std::runtime_error("not enough data");
    }
    const char *data = std::get<0>(tuple);
    std::memcpy(&gyro, data + 0, 2);
    std::memcpy(&accel, data + 2, 2);
    return {gyro, accel};
}

void rg::lego_gyro::calibrate() {
    gyro_mode old = getMode();
    if (old == gyro_mode::GYRO_RATE) {
        setMode(gyro_mode::GYRO_ANG);
    } else {
        setMode(gyro_mode::GYRO_RATE);
    }
    idle_task::thread_sleep(250);
    setMode(old);
    idle_task::thread_sleep(250);
}
