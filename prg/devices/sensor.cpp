//
// Created by kuba on 18.3.19.
//

#include "sensor.hpp"
#include "lego_color.hpp"
#include "lego_ultrasonic.hpp"
#include "lego_gyro.hpp"
#include "lego_touch.hpp"

rg::sensor::sensor(rg::device_ptr dev)
        : dev(dev),
          a_address(dev->attr("address")),
          a_bin_data(dev->attr("bin_data")),
          a_bin_data_format(dev->attr("bin_data_format")),
          a_decimals(dev->attr("decimals")),
          a_driver_name(dev->attr("driver_name")),
          a_mode(dev->attr("mode")),
          a_modes(dev->attr("modes")),
          a_num_values(dev->attr("num_values")),
          a_value0(dev->attr("value0")),
          a_value1(dev->attr("value1")),
          a_value2(dev->attr("value2")),
          a_value3(dev->attr("value3")),
          a_value4(dev->attr("value4")),
          a_value5(dev->attr("value5")),
          a_value6(dev->attr("value6")),
          a_value7(dev->attr("value7")) {

}

rg::attribute_ptr &rg::sensor::a_valueN(int n) {
    switch (n) {
        case 0:
            return a_value0;
        case 1:
            return a_value1;
        case 2:
            return a_value2;
        case 3:
            return a_value3;
        case 4:
            return a_value4;
        case 5:
            return a_value5;
        case 6:
            return a_value6;
        case 7:
            return a_value7;
        default:
            throw std::logic_error("Invalid value number");
    }
}

std::string rg::sensor::getAddress() {
    return a_address->readString();
}

rg::sensor_type rg::sensor::getSensorType() {
    std::string type = a_driver_name->readString();
    if (type == lego_color::driver) {
        return sensor_type::color;
    } else if (type == lego_gyro::driver) {
        return sensor_type::gyro;
    } else if (type == lego_ultrasonic::driver) {
        return sensor_type::ultrasonic;
    } else if (type == lego_touch::driver) {
        return sensor_type::touch;
    } else {
        return sensor_type::unknown;
    }
}

std::tuple<const char *, int> rg::sensor::readBinaryData() {
    const char *data = a_bin_data->read();
    int len = a_bin_data->lastReadLength();
    return {data, len};
}

std::string rg::sensor::binaryDataFormat() {
    return a_bin_data_format->readString();
}

int rg::sensor::getDecimals() {
    return a_decimals->readInt();
}

std::string rg::sensor::readValueString(int n) {
    return a_valueN(n)->readString();
}

int rg::sensor::readValueInt(int n) {
    return a_valueN(n)->readInt();
}

float rg::sensor::readValueFloat(int n) {
    return a_valueN(n)->readFloat();
}

float rg::sensor::readValueFloatScaled(int n) {
    return readValueFloat(n) / getDecimals();
}

std::vector<std::string> rg::sensor::getModes() {
    return a_modes->readStringList();
}

std::string rg::sensor::getModeString() {
    return a_mode->readString();
}

void rg::sensor::setModeString(const std::string &mode) {
    a_mode->writeString(mode);
}

int rg::sensor::getNumberOfValues() {
    return a_num_values->readInt();
}
