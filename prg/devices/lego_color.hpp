//
// Created by kuba on 18.3.19.
//

#ifndef PRG_LEGO_COLOR_HPP
#define PRG_LEGO_COLOR_HPP

#include "sensor.hpp"

namespace rg {
    enum class color_mode {
        COL_REFLECT,
        COL_AMBIENT,
        COL_COLOR,
        REF_RAW,
        RGB_RAW,
        COL_CAL
    };

    class lego_color : public sensor {
    public:
        static constexpr const char *name = "EV3 Color Sensor";
        static constexpr const char *driver = "lego-ev3-color";

        explicit lego_color(device_ptr ptr);

        void setMode(color_mode mode);

        color_mode getMode();

        uint8_t readReflection();
    };
}


#endif //PRG_LEGO_COLOR_HPP
