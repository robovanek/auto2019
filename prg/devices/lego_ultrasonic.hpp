//
// Created by kuba on 18.3.19.
//

#ifndef PRG_LEGO_ULTRASONIC_HPP
#define PRG_LEGO_ULTRASONIC_HPP

#include "devices.hpp"
#include "sensor.hpp"

namespace rg {
    enum class ultrasonic_mode {
        distance_cm,
        distance_in,
        listen,
        oneshot_cm,
        oneshot_in,
        special_cm,
        special_in
    };

    class lego_ultrasonic : public sensor {
    public:
        static constexpr const char *name = "EV3 Ultrasonic Sensor";
        static constexpr const char *driver = "lego-ev3-us";

        explicit lego_ultrasonic(device_ptr ptr);

        void setMode(ultrasonic_mode mode);

        ultrasonic_mode getMode();

        uint16_t readDistance();

        bool readListen();
    };
}


#endif //PRG_LEGO_ULTRASONIC_HPP
