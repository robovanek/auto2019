#include <utility>

//
// Created by kuba on 18.3.19.
//

#include <cstring>
#include "lego_ultrasonic.hpp"

rg::lego_ultrasonic::lego_ultrasonic(rg::device_ptr ptr) : sensor(std::move(ptr)) {}

void rg::lego_ultrasonic::setMode(rg::ultrasonic_mode mode) {
    std::string modeStr;
    switch (mode) {
        case ultrasonic_mode::distance_cm:
            modeStr = "US-DIST-CM";
            break;
        case ultrasonic_mode::distance_in:
            modeStr = "US-DIST-IN";
            break;
        case ultrasonic_mode::listen:
            modeStr = "US-LISTEN";
            break;
        case ultrasonic_mode::oneshot_cm:
            modeStr = "US-SI-CM";
            break;
        case ultrasonic_mode::oneshot_in:
            modeStr = "US-SI-IN";
            break;
        case ultrasonic_mode::special_cm:
            modeStr = "US-DC-CM";
            break;
        case ultrasonic_mode::special_in:
            modeStr = "US-DC-IN";
            break;
    }
    setModeString(modeStr);
}

rg::ultrasonic_mode rg::lego_ultrasonic::getMode() {
    std::string modeStr = getModeString();
    if (modeStr == "US-DIST-CM") {
        return ultrasonic_mode::distance_cm;
    } else if (modeStr == "US-DIST-IN") {
        return ultrasonic_mode::distance_in;
    } else if (modeStr == "US-LISTEN") {
        return ultrasonic_mode::listen;
    } else if (modeStr == "US-SI-CM") {
        return ultrasonic_mode::oneshot_cm;
    } else if (modeStr == "US-SI-IN") {
        return ultrasonic_mode::oneshot_in;
    } else if (modeStr == "US-DC-CM") {
        return ultrasonic_mode::special_cm;
    } else if (modeStr == "US-DC-IN") {
        return ultrasonic_mode::special_in;
    } else {
        throw std::runtime_error("unknown ultrasonic sensor mode: " + modeStr);
    }
}

uint16_t rg::lego_ultrasonic::readDistance() {
    return readBinaryInteger<uint16_t>(readBinaryData());
}

bool rg::lego_ultrasonic::readListen() {
    return readBinaryInteger<uint8_t>(readBinaryData()) != 0;
}
