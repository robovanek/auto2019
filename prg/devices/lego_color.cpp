#include <utility>

//
// Created by kuba on 18.3.19.
//

#include <cstring>
#include "lego_color.hpp"

rg::lego_color::lego_color(device_ptr ptr) : sensor(std::move(ptr)) {}

void rg::lego_color::setMode(rg::color_mode mode) {
    std::string modeStr;
    switch (mode) {
        case color_mode::COL_REFLECT:
            modeStr = "COL-REFLECT";
            break;
        case color_mode::COL_AMBIENT:
            modeStr = "COL-AMBIENT";
            break;
        case color_mode::COL_COLOR:
            modeStr = "COL-COLOR";
            break;
        case color_mode::REF_RAW:
            modeStr = "REF-RAW";
            break;
        case color_mode::RGB_RAW:
            modeStr = "RGB-RAW";
            break;
        case color_mode::COL_CAL:
            modeStr = "COL-CAL";
            break;
    }
    setModeString(modeStr);
}

rg::color_mode rg::lego_color::getMode() {
    std::string modeStr = getModeString();
    if (modeStr == "COL-REFLECT") {
        return color_mode::COL_REFLECT;
    } else if (modeStr == "COL-AMBIENT") {
        return color_mode::COL_AMBIENT;
    } else if (modeStr == "COL-COLOR") {
        return color_mode::COL_COLOR;
    } else if (modeStr == "REF-RAW") {
        return color_mode::REF_RAW;
    } else if (modeStr == "RGB-RAW") {
        return color_mode::RGB_RAW;
    } else if (modeStr == "COL-CAL") {
        return color_mode::COL_CAL;
    } else {
        throw std::runtime_error("unknown color sensor mode: " + modeStr);
    }
}

uint8_t rg::lego_color::readReflection() {
    return readBinaryInteger<uint8_t>(readBinaryData());
}

