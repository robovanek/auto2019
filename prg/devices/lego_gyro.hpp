//
// Created by kuba on 18.3.19.
//

#ifndef PRG_LEGO_GYRO_HPP
#define PRG_LEGO_GYRO_HPP

#include "sensor.hpp"

namespace rg {
    enum class gyro_mode {
        GYRO_ANG,
        GYRO_RATE,
        GYRO_FAS,
        GYRO_GnA,
        GYRO_CAL
    };

    class lego_gyro : public sensor {
    public:
        static constexpr const char *name = "EV3 Gyro Sensor";
        static constexpr const char *driver = "lego-ev3-gyro";

        explicit lego_gyro(device_ptr ptr);

        void setMode(gyro_mode mode);

        gyro_mode getMode();

        void calibrate();

        int16_t readAngle();

        int16_t readRate();

        std::tuple<int16_t, int16_t> readAngleAndRate();
    };
}


#endif //PRG_LEGO_GYRO_HPP
