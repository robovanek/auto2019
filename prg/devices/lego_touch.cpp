#include <utility>

//
// Created by kuba on 18.3.19.
//

#include "lego_touch.hpp"

rg::lego_touch::lego_touch(rg::device_ptr ptr) : sensor(std::move(ptr)) {}

bool rg::lego_touch::isPressed() {
    return a_value0->read()[0] == '1';
}

void rg::lego_touch::setMode(touch_mode mode) {
    setModeString("TOUCH");
}

rg::touch_mode rg::lego_touch::getMode() {
    std::string modeStr = getModeString();
    if (modeStr == "TOUCH") {
        return touch_mode::TOUCH;
    } else {
        throw std::runtime_error("unknown touch sensor mode: " + modeStr);
    }
}
