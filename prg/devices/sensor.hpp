//
// Created by kuba on 18.3.19.
//

#ifndef PRG_SENSOR_HPP
#define PRG_SENSOR_HPP

#include "devices.hpp"

namespace rg {

    enum class sensor_type {
        gyro,
        touch,
        ultrasonic,
        color,
        unknown
    };

    class sensor {
    public:
        explicit sensor(device_ptr dev);

        virtual ~sensor() = default;

        std::string getAddress();

        sensor_type getSensorType();

        std::tuple<const char *, int> readBinaryData();

        std::string binaryDataFormat();

        int getDecimals();

        std::string readValueString(int n);

        int readValueInt(int n);

        float readValueFloat(int n);

        float readValueFloatScaled(int n);

        std::vector<std::string> getModes();

        std::string getModeString();

        void setModeString(const std::string &mode);

        int getNumberOfValues();

    protected:
        device_ptr dev;

        attribute_ptr &a_valueN(int n);

        attribute_ptr a_address; // ok
        attribute_ptr a_bin_data; // ok
        attribute_ptr a_bin_data_format; // ok
        attribute_ptr a_decimals; // ok
        attribute_ptr a_driver_name; // ok
        attribute_ptr a_mode; // ok
        attribute_ptr a_modes; // ok
        attribute_ptr a_num_values; // ok
        attribute_ptr a_value0; // ok
        attribute_ptr a_value1; // ok
        attribute_ptr a_value2; // ok
        attribute_ptr a_value3; // ok
        attribute_ptr a_value4; // ok
        attribute_ptr a_value5; // ok
        attribute_ptr a_value6; // ok
        attribute_ptr a_value7; // ok
    };
}


#endif //PRG_SENSOR_HPP
