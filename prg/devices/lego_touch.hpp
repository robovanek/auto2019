//
// Created by kuba on 18.3.19.
//

#ifndef PRG_LEGO_TOUCH_HPP
#define PRG_LEGO_TOUCH_HPP

#include "sensor.hpp"

namespace rg {
    enum class touch_mode {
        TOUCH
    };

    class lego_touch : public sensor {
    public:
        static constexpr const char *name = "EV3 Touch Sensor";
        static constexpr const char *driver = "lego-ev3-touch";

        explicit lego_touch(device_ptr ptr);

        void setMode(touch_mode mode);

        touch_mode getMode();

        bool isPressed();
    };
}


#endif //PRG_LEGO_TOUCH_HPP
