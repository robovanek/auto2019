cmake_minimum_required(VERSION 3.7)
project(robogympl)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/modules")

set(CMAKE_CXX_STANDARD 11)

find_package(udev)
if (NOT UDEV_FOUND)
    message(FATAL_ERROR "UDEV not found")
else ()
    message(STATUS "UDEV lib:      ${UDEV_LIBRARY}")
    message(STATUS "UDEV includes: ${UDEV_INCLUDE_DIR}")
endif ()

add_executable(robogympl
        src/program.cpp
        src/field_map.cpp
        src/scheduler.hpp
        src/scheduler.cpp
        src/planner.hpp
        src/planner.cpp
        src/program.hpp
        src/field_map.hpp
        src/measurements.cpp
        src/measurements.hpp
        src/main.cpp
        udev/Connection.cpp
        udev/Connection.h
        udev/Attribute.h
        udev/Attribute.cpp
        udev/Common.h
        udev/Common.cpp
        udev/Device.h
        udev/Device.cpp
        udev/Registry.cpp
        udev/Registry.hpp
        robofat/util.h
        robofat/util.cpp
        robofat/display.h
        robofat/display.cpp
        robofat/pid.cpp
        robofat/pid.h
        robofat/robofat.h
        robofat/settings.cpp
        robofat/settings.h
        robofat/pid_param.h
        devices/motor.cpp
        devices/lego_ultrasonic.cpp
        devices/lego_color.cpp
        devices/lego_gyro.cpp
        devices/motor.hpp
        devices/sensor.hpp
        devices/sensor.cpp
        devices/devices.hpp
        devices/lego_ultrasonic.hpp
        devices/lego_gyro.hpp
        devices/lego_color.hpp
        devices/lego_touch.hpp
        devices/lego_touch.cpp
        src/odometry.cpp
        src/odometry.hpp
        src/device_store.cpp
        src/device_store.hpp
        src/path_finder.cpp
        src/path_finder.hpp
        src/mechanics.cpp
        src/mechanics.hpp
        src/environment.cpp
        src/environment.hpp
        src/exceptions.hpp src/exceptions.cpp src/moves.cpp src/moves.hpp src/rawreg.cpp src/rawreg.hpp src/linear_ramp.cpp src/linear_ramp.hpp src/angular_ramp.cpp src/angular_ramp.hpp)
target_link_libraries(robogympl ${UDEV_LIBRARY} backtrace)
target_include_directories(robogympl PRIVATE
        ${UDEV_INCLUDE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        ${CMAKE_CURRENT_SOURCE_DIR}/udev
        ${CMAKE_CURRENT_SOURCE_DIR}/robofat
        ${CMAKE_CURRENT_SOURCE_DIR}/devices)

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -s")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")

if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm")
    add_definitions (robogympl -funwind-tables)
endif()