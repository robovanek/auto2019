/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O implementation.
 */


#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <Common.h>
#include "display.h"
#include "util.h"
#include <linux/input.h>
#include <scheduler.hpp>

static const char *io_path = "/dev/input/by-path/platform-gpio_keys-event";
static constexpr size_t io_len = (KEY_MAX + 7) / 8;

robofat::display::display() : m_fd(-1), m_keyBuffer(io_len, 0x00) {
    if ((m_fd = ::open(io_path, O_RDONLY)) < 0) {
        ev3udev::raise_errno("open", io_path);
    }
}


robofat::display::~display() {
    if (m_fd >= 0) {
        if (::close(m_fd) < 0) {
            ev3udev::raise_errno("close", io_path);
        }
    }
}

robofat::display::keys robofat::display::getKeys() {
    int retval = ::ioctl(m_fd, EVIOCGKEY(io_len), m_keyBuffer.data());
    if (retval < 0) {
        ev3udev::raise_errno("ioctl", io_path);
    }
    keys result = keys::key_none;
    addIfPressed(result, KEY_LEFT, keys::key_left);
    addIfPressed(result, KEY_RIGHT, keys::key_right);
    addIfPressed(result, KEY_UP, keys::key_up);
    addIfPressed(result, KEY_DOWN, keys::key_down);
    addIfPressed(result, KEY_ENTER, keys::key_enter);
    addIfPressed(result, KEY_BACKSPACE, keys::key_escape);
    return result;
}

bool robofat::display::keyPressed(int no) {
    int byte = no / 8;
    int bit = no % 8;
    return (m_keyBuffer[byte] & (1 << bit)) != 0;
}

void robofat::display::addIfPressed(robofat::display::keys &store, int no, robofat::display::keys key) {
    if (keyPressed(no)) {
        store = (keys) (store | key);
    }
}

long robofat::display::show_menu(const std::string &prompt,
                                 const std::vector<std::string> &options,
                                 long select) {
    // zero options is not valid
    if (options.empty())
        return -1;
    long sMin = 0;
    long sMax = options.size() - 1;

    select = robofat::utility::limit(select, sMin, sMax);

    // event loop
    while (true) {
        // draw
        menu_draw(prompt, options, select);

        // get input
        keys key;
        do {
            key = getKeys();
            rg::idle_task::thread_sleep(100);
        } while (key == key_none);

        // process input
        switch (key) {
            case key_up:
                select--;
                if (select < sMin)
                    select = sMax;
                break;
            case key_down:
                select++;
                if (select > sMax)
                    select = sMin;
                break;
            case key_enter:
                return select;
            case key_escape:
            case key_left:
            case key_right:
            case key_none:
                break;
        }
    }
}

void robofat::display::menu_draw(const std::string &prompt,
                                 const std::vector<std::string> &options,
                                 long select) {
    std::cout << std::endl;
    std::cout << prompt << std::endl;
    for (int i = 0; i < options.size(); i++) {
        if (i == select) {
            std::cout << "=> ";
        } else {
            std::cout << "   ";
        }
        std::cout << options[i] << std::endl;
    }
    std::cout << std::flush;
}
