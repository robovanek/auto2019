/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Aggregate include and documentation header.
 */

#ifndef ROBOFAT_ROBOFAT_H
#define ROBOFAT_ROBOFAT_H

#include "pid.h"
#include "pid_param.h"
#include "settings.h"
#include "util.h"

/**
 * \brief Robotic Flying Apple Tree (RoboFAT), a small C++ robotic library for ev3dev
 *
 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
 */
namespace robofat {

}

namespace rg {
    using namespace robofat;
    using namespace robofat::utility;
}

#endif //ROBOFAT_ROBOFAT_H
