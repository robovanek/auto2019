/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O.
 */

#ifndef ROBOFAT_DISPLAY_H
#define ROBOFAT_DISPLAY_H

#include <string>
#include <vector>

namespace robofat {
    /**
     * \brief Screen I/O
     *
     * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
     */
    class display {
    public:
        /**
         * \brief EV3 keys
         */
        enum keys {
            /**
             * \brief Unknown key
             */
                    key_none = 0x00,
            /**
             * \brief The up arrow button.
             */
                    key_up = 0x01,
            /**
             * \brief The down arrow button.
             */
                    key_down = 0x02,
            /**
             * \brief The left arrow button.
             */
                    key_left = 0x04,
            /**
             * \brief The right arrow button.
             */
                    key_right = 0x08,
            /**
             * \brief The confirm (center) button.
             */
                    key_enter = 0x10,
            /**
             * \brief The leave (out) button.
             */
                    key_escape = 0x20
        };

        /**
         * \brief Initializes ncurses.
         */
        display();

        /**
         * \brief Deinitializes ncurses.
         */
        ~display();

        /**
         * \brief Read an EV3 button from main window.
         *
         * @return      The pressed button or `key_unknown`.
         */
        keys getKeys();

        /**
         * \brief Start an interactive menu and return what the user has selected.
         *
         * @param prompt    Selection prompt.
         * @param options   Available options.
         * @param select    Default option index.
         * @param can_exit  Whether the user can cancel the selection with EXIT button.
         * @return          Index of selected option or -1 if user cancelled the menu.
         */
        long show_menu(const std::string &prompt,
                       const std::vector<std::string> &options,
                       long select = 0);

    private:
        /**
         * \brief Draw a menu
         *
         * @param wnd           Window for drawing the menu.
         * @param prompt        Selection prompt.
         * @param options       Available options.
         * @param select        Index of selected option.
         */
        void menu_draw(const std::string &prompt,
                       const std::vector<std::string> &options,
                       long select);

        int m_fd;
        std::vector<uint8_t> m_keyBuffer;

        bool keyPressed(int no);

        void addIfPressed(keys &store, int no, keys key);
    };
}

#endif //ROBOFAT_DISPLAY_H
