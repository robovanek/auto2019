/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Settings storage implementation.
 */

#include <algorithm>
#include "settings.h"
#include "util.h"

using namespace std;
using robofat::utility::ltrim;
using robofat::utility::rtrim;
using robofat::utility::trim;


robofat::settings::settings(const std::string &file, bool lower_keys, bool lower_values) : values() {
    std::ifstream ifile(file, std::ios::in);
    if (!load(ifile, lower_keys, lower_values)) {
        throw std::runtime_error("Loading config file failed.");
    }
    ifile.close();
}

bool robofat::settings::load(istream &input, bool lower_keys, bool lower_values) {
    string line;
    while (getline(input, line)) {
        // outer trim
        trim(line);
        // ignore empty lines and comment lines
        if (line.empty() || line.at(0) == '#')
            continue;
        // find further comments
        size_t comment = line.find('#');
        if (comment != string::npos) {
            // cut the comment out
            line.erase(comment);
            // outer trim
            rtrim(line);
        }
        // get key and value
        size_t equals = line.find('=');
        if (equals == string::npos)
            continue;
        string key = line.substr(0, equals);
        string value = line.substr(equals + 1);
        // inner trim
        rtrim(key);
        ltrim(value);
        // go to lower
        if (lower_keys)
            transform(key.begin(), key.end(), key.begin(), ::tolower);
        if (lower_values)
            transform(value.begin(), value.end(), value.begin(), ::tolower);
        values.emplace(key, value);
    }
    return !(input.bad() || !input.eof());
}

std::string robofat::settings::port_java2cpp(std::string lejosName) {
    transform(lejosName.begin(), lejosName.end(), lejosName.begin(), ::tolower);
    if (lejosName == "a") {
        return "ev3-ports:outA";
    } else if (lejosName == "b") {
        return "ev3-ports:outB";
    } else if (lejosName == "c") {
        return "ev3-ports:outC";
    } else if (lejosName == "d") {
        return "ev3-ports:outD";

    } else if (lejosName == "s1") {
        return "ev3-ports:in1";
    } else if (lejosName == "s2") {
        return "ev3-ports:in2";
    } else if (lejosName == "s3") {
        return "ev3-ports:in3";
    } else if (lejosName == "s4") {
        return "ev3-ports:in4";
    }
    throw std::runtime_error("Unknown port name: " + lejosName);
}

double robofat::settings::get_double(const std::string &name) const {
    return stod(values.at(name));
}

float robofat::settings::get_float(const std::string &name) const {
    return stof(values.at(name));
}

int robofat::settings::get_integer(const std::string &name) const {
    return stoi(values.at(name));
}

std::string robofat::settings::get(const std::string &name) const {
    return values.at(name);
}

bool robofat::settings::get_bool(const std::string &name) const {
    std::string value = values.at(name);
    transform(value.begin(), value.end(), value.begin(), ::tolower);
    return value != "false" && value != "0";
}

std::string robofat::settings::get_port(const std::string &name) const {
    return port_java2cpp(values.at(name));
}

bool robofat::settings::has_key(const string &name) const {
    return values.find(name) == values.end();
}

