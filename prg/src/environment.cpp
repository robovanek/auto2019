//
// Created by kuba on 21.3.19.
//

#include "environment.hpp"
#include "program.hpp"

rg::environment::environment(program &prg)
        : task("environment measurement processor", prg),
          m_sonicSideSpace(m_prg->cfg.get_integer("sonic side space")),
          m_colorThreshold(m_prg->cfg.get_integer("color threshold")),
          m_sonicMaxFree(m_prg->cfg.get_integer("sonic max free")),
          m_colorOnRight(m_prg->cfg.get_bool("color on right")) {}

void rg::environment::update() {
    // todo environment monitoring
    // like fuckup detection etc.
    if (m_counter > 50) {
        m_counter = 0;
        print();
    } else {
        m_counter++;
    }
}

void rg::environment::writeSides() {
    rotation colorRot = m_colorOnRight ? rotation::right : rotation::left;
    rotation sonicRot = m_colorOnRight ? rotation::left : rotation::right;

    orient colorDir = rg::rotate(direction, colorRot);
    orient sonicDir = rg::rotate(direction, sonicRot);

    // hopefully
    pos colorPos = position.advance(colorDir, 1);
    if (m_prg->msr.redReflection > m_colorThreshold) {
        std::cerr << "[" << colorPos.x() << "," << colorPos.y() << "] wall" << std::endl << std::flush;
        m_prg->map.set(colorPos, tile::wall);
    } else {
        std::cerr << "[" << colorPos.x() << "," << colorPos.y() << "] free" << std::endl << std::flush;
        m_prg->map.set(colorPos, tile::free);
    }

    int freeBlocks = (m_prg->msr.sonicDistance - m_sonicSideSpace + 140) / 280;
    if (freeBlocks == 0) {
        pos p = position.advance(sonicDir, 1);
        std::cerr << "[" << p.x() << "," << p.y() << "] wall" << std::endl << std::flush;
        m_prg->map.set(p, tile::wall);
    } else {
        for (int i = 1; i <= freeBlocks && i <= m_sonicMaxFree; i++) {
            pos p = position.advance(sonicDir, i);
            std::cerr << "[" << p.x() << "," << p.y() << "] free" << std::endl << std::flush;
            m_prg->map.set(p, tile::free);
        }
    }
}

void rg::environment::writeWallHit() {
    m_prg->map.set(position.advance(direction, 1), tile::wall);
}

void rg::environment::advanceForward() {
    position = position.advance(direction, +1);
    std::cerr << "[env] moved to [" << position.x() << "," << position.y() << "]" << std::endl << std::flush;
}

void rg::environment::advanceBackward() {
    std::cerr << "" << std::endl << std::flush;
    position = position.advance(direction, -1);
    std::cerr << "[env] moved to [" << position.x() << "," << position.y() << "]" << std::endl << std::flush;
}

void rg::environment::rotate(rg::rotation rot) {
    direction = rg::rotate(direction, rot);
    if (rot == rg::rotation::left) {
        std::cerr << "[env] turned left" << std::endl << std::flush;
    } else {
        std::cerr << "[env] turned right" << std::endl << std::flush;
    }
}

void rg::environment::print() {
    for (int y = -1; y <= field_map::height; y++) {
        for (int x = -1; x <= field_map::width; x++) {
            pos p(x, y);
            const char *ch = nullptr;
            if (p == this->position) {
                switch (this->direction) {
                    case orient::north:
                        ch = "↑";
                        break;
                    case orient::south:
                        ch = "↓";
                        break;
                    case orient::east:
                        ch = "→";
                        break;
                    case orient::west:
                        ch = "←";
                        break;
                }
            } else {
                tile t = m_prg->map.get(p);
                switch (t) {
                    case tile::unknown:
                        ch = "?";
                        break;
                    case tile::wall:
                        ch = "█";
                        break;
                    case tile::free:
                        ch = " ";
                        break;
                }
            }
            std::cout << ch;
        }
        std::cout << std::endl;
    }
    std::cout << std::flush;
}
