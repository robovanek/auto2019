//
// Created by kuba on 25.3.19.
//

#include "moves.hpp"
#include "program.hpp"

//////////////////
// GENERIC MOVE //
//////////////////

rg::move_state::move_state(rg::program &prg, const std::string &name, bool needsCage)
        : m_prg(&prg),
          m_name(name),
          m_exitReason(finish_reason::movement_finished),
          m_continue(true),
          m_needsCage(needsCage),
          m_started(false) {}

void rg::move_state::stallDetected() {
    std::cerr << "[mechanics] motors stall" << std::endl << std::flush;

    m_prg->mreg.stop();
    finish(finish_reason::wheel_stall);
}

void rg::move_state::touchDetected() {
    std::cerr << "[mechanics] touch pressed" << std::endl << std::flush;

    m_prg->mreg.stop();
    finish(finish_reason::touch_pressed);
}

void rg::move_state::onExit() {
}

bool rg::move_state::needsCageDown() {
    return m_needsCage;
}

void rg::move_state::signalStart() {
    m_started = true;
}

bool rg::move_state::hasStarted() {
    return m_started;
}

float rg::move_state::timeCalc() {
    timestamp now = {};
    timestamp diff = {};
    tsGet(now);
    tsDiff(m_last, now, diff);
    m_last = now;
    return tsMsec(diff);
}

/////////////////////////
// SIMPLE MOVE FORWARD //
/////////////////////////

rg::line_move::line_move(rg::program &prg, float mm, bool forward, int speed, bool reactToTouch)
        : move_state(prg, "linear move", !forward),
          m_forward(forward),
          m_speed(speed),
          m_touch(reactToTouch) {
    m_infinite = !std::isfinite(mm);
    if (m_infinite) {
        m_degrees = INT32_MAX;
    } else {
        m_degrees = m_prg->mdata.linearMoveDegrees(mm);
    }
}

rg::line_move::line_move(rg::program &prg, float mm)
        : line_move(prg, mm, true, prg.mdata.fwdSpeed, true) {}

void rg::line_move::onEnter() {
    signalStart();

    m_left0 = m_prg->msr.tachoLeft;
    m_right0 = m_prg->msr.tachoRight;

    if (m_infinite) {
        m_leftSP = m_forward ? INT32_MAX : INT32_MIN;
        m_rightSP = m_forward ? INT32_MAX : INT32_MIN;
    } else {
        m_leftSP = m_left0 + m_degrees * inversion_sign(!m_forward);
        m_rightSP = m_right0 + m_degrees * inversion_sign(!m_forward);
    }

    m_prg->mreg.start();
}

void rg::line_move::update() {
    float travelled = m_prg->lramp.travelled(m_left0, m_right0);
    float remaining = m_prg->lramp.remaining(m_leftSP, m_rightSP);

    if (travelled >= m_degrees) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }

    float rq = m_prg->lramp.rampQuotient(travelled, remaining, m_speed) * m_speed * inversion_sign(!m_forward);

    m_prg->mreg.setRequest(rq, rq);
}

void rg::line_move::touchDetected() {
    if (m_touch) {
        move_state::touchDetected();
    }
}


////////////////
// TIMED MOVE //
////////////////

rg::line_move_timed::line_move_timed(rg::program &prg, int msecs, bool forward, int speed)
        : line_move(prg, prg.mdata.moveMillimeters((float) speed * msecs / 1000.0f), forward, speed, true) {}

///////////////////////////
// FORWARD WALL FOLLOWER //
///////////////////////////


rg::forward_wall_move::forward_wall_move(rg::program &prg, float mm)
        : forward_wall_move(prg, mm, prg.mdata.fwdSpeed) {}

rg::forward_wall_move::forward_wall_move(rg::program &prg, float mm, float speed)
        : move_state(prg, "regulator experiment", false),
          m_sonicPID(m_prg->mdata.fwdRegSonicPIDa, m_prg->mdata.fwdRegSonicPIDb),
          m_colorPID(m_prg->mdata.fwdRegColorPIDa, m_prg->mdata.fwdRegColorPIDb),
          m_colorMin(m_prg->mdata.fwdRegColorMin),
          m_colorMax(m_prg->mdata.fwdRegColorMax),
          m_sonicMin(m_prg->mdata.fwdRegSonicMin),
          m_sonicMax(m_prg->mdata.fwdRegSonicMax),
          m_sonicSP(m_prg->mdata.fwdRegSonicSetpoint),
          m_colorSP(m_prg->mdata.fwdRegColorSetpoint),
          m_speed(speed),
          m_colorSqrt(m_prg->mdata.fwdRegColorSqrt),
          m_relativeMax(m_prg->mdata.fwdRegRelMax),
          m_relativeMin(m_prg->mdata.fwdRegRelMin),
          m_sonicBug1MaxFix(prg.cfg.get_float("fwd regulator sonic maxfix")),
          m_sonicHistoryLength(prg.cfg.get_integer("fwd regulator sonic log length")),
          m_sonicBug2Tolerance(prg.cfg.get_float("fwd regulator sonic log tolerance")) {
    m_degrees = prg.mdata.linearMoveDegrees(mm);
}

void rg::forward_wall_move::onEnter() {
    signalStart();

    m_left0 = m_prg->msr.tachoLeft;
    m_right0 = m_prg->msr.tachoRight;

    m_leftSP = m_left0 + m_degrees;
    m_rightSP = m_right0 + m_degrees;

    m_prg->mreg.start();
    timeCalc();
}

void rg::forward_wall_move::update() {
    float elapsed = timeCalc();
    float travelled = m_prg->lramp.travelled(m_left0, m_right0);
    float remaining = m_prg->lramp.remaining(m_leftSP, m_rightSP);

    if (travelled >= m_degrees) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }


    float control = regulateColor(elapsed) + regulateSonic(elapsed);

    float speedL = m_speed + control;
    float speedR = m_speed - control;
    float speedMax = m_speed * m_relativeMax;
    float speedMin = m_speed * m_relativeMin;


    float base = m_prg->lramp.rampQuotient(travelled, remaining, m_speed);

    float rqL = limit(speedL, speedMin, speedMax) * base;
    float rqR = limit(speedR, speedMin, speedMax) * base;

    bool overloaded = !is_bounded(speedL, speedMin, speedMax) ||
                      !is_bounded(speedR, speedMin, speedMax);
    m_colorPID.set_i_freeze(overloaded);
    m_sonicPID.set_i_freeze(overloaded);

    m_prg->mreg.setRequest(rqL, rqR);
}

float rg::forward_wall_move::regulateSonic(float elapsed) {
    int msr = m_prg->msr.sonicDistance;
    addSonicMeasurement(msr);
    float avg = getSonicAverage();

    bool present = is_bounded(msr, m_sonicMin, m_sonicMax);
    bool inRange = is_bounded((float) msr, avg - m_sonicBug2Tolerance, avg + m_sonicBug2Tolerance);

    if (present && inRange) {
        float error = m_sonicSP - msr;
        float fix = m_sonicPID.process(error, elapsed);
        if (fabsf(fix) > m_sonicBug1MaxFix) {
            return 0.0f;
        } else {
            return fix;
        }
    } else {
        return 0.0f;
    }
}

float rg::forward_wall_move::regulateColor(float elapsed) {
    bool present = is_bounded(m_prg->msr.redReflection,
                              m_colorMin,
                              m_colorMax);
    if (present) {
        float error = m_colorSP - m_prg->msr.redReflection;
        if (m_colorSqrt) {
            float sign = signum(error);
            error = sign * sqrtf(fabsf(error));
        }
        return m_colorPID.process(error, elapsed);
    } else {
        return 0.0f;
    }
}

void rg::forward_wall_move::stallDetected() {
    // fixme ignore
}

void rg::forward_wall_move::onExit() {
    // noop, no kernel regulation
}

void rg::forward_wall_move::touchDetected() {
    m_prg->mreg.stop();
    finish(finish_reason::touch_pressed);
}

void rg::forward_wall_move::addSonicMeasurement(float f) {
    if (m_sonicHistory.empty()) {
        for (int i = 0; i < m_sonicHistoryLength; i++) {
            m_sonicHistory.push_back(f);
        }
    } else {
        m_sonicHistory.pop_front();
        m_sonicHistory.push_back(f);
    }
}

float rg::forward_wall_move::getSonicAverage() {
    float sum = 0.0f;
    for (float f : m_sonicHistory) {
        sum += f;
    }
    return sum / m_sonicHistoryLength;
}


///////////////////////////
// NON-REGULATED TURNING //
///////////////////////////

rg::turn_normal::turn_normal(rg::program &prg, float degreesCCW, int rotSpeed)
        : move_state(prg, "non-regulated turning", false) {
    m_ccw = degreesCCW > 0.0f;
    m_angleAbs = ftoi(fabsf(degreesCCW * prg.mdata.trackRadius / prg.mdata.wheelRadius));
    m_speedAbs = abs(rotSpeed);
}

void rg::turn_normal::update() {
    float travelled = m_prg->lramp.travelled(m_left0, m_right0);
    float remaining = m_prg->lramp.remaining(m_leftSP, m_rightSP);

    if (travelled >= m_angleAbs) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }

    float rq = m_prg->lramp.rampQuotient(travelled, remaining, m_speedAbs) * m_speedAbs;

    m_prg->mreg.setRequest(m_ccw ? -rq : +rq,
                           m_ccw ? +rq : -rq);
}

void rg::turn_normal::onEnter() {
    signalStart();

    m_left0 = m_prg->msr.tachoLeft;
    m_right0 = m_prg->msr.tachoRight;

    m_leftSP = m_left0 + (m_ccw ? -m_angleAbs : +m_angleAbs);
    m_rightSP = m_right0 + (m_ccw ? +m_angleAbs : -m_angleAbs);

    m_prg->mreg.start();
}


////////////////////////////
// NON-REGULATED STEERING //
////////////////////////////

rg::steer_normal::steer_normal(rg::program &prg,
                               float radius,
                               float degreesCCW,
                               bool forward,
                               int speed)
        : move_state(prg, "non-regulated steering", !forward) {
    m_forward = forward;
    m_ccw = degreesCCW > 0;

    float shorterS = (radius) / (radius + prg.mdata.trackRadius) * speed;
    float longerS = (radius + 2 * prg.mdata.trackRadius) / (radius + prg.mdata.trackRadius) * speed;

    float common = fabsf(degreesCCW) / prg.mdata.wheelRadius;
    float shorterP = common * (radius);
    float longerP = common * (radius + 2 * prg.mdata.trackRadius);

    int inverter = inversion_sign(!forward);
    m_angleL = (int) (m_ccw ? shorterP : longerP) * inverter;
    m_speedL = (int) (m_ccw ? shorterS : longerS) * inverter;
    m_angleR = (int) (m_ccw ? longerP : shorterP) * inverter;
    m_speedR = (int) (m_ccw ? longerS : shorterS) * inverter;
}

void rg::steer_normal::update() {
    int travelL = abs(m_prg->msr.tachoLeft - m_left0);
    int travelR = abs(m_prg->msr.tachoRight - m_left0);
    int remainL = abs(m_leftSP - m_prg->msr.tachoLeft);
    int remainR = abs(m_rightSP - m_prg->msr.tachoRight);

    if (travelL >= abs(m_angleL) && travelR >= abs(m_angleR)) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }

    float q;
    if (m_ccw) {
        q = m_prg->lramp.rampQuotient(travelR, remainR, abs(m_speedR));
    } else {
        q = m_prg->lramp.rampQuotient(travelL, remainL, abs(m_speedL));
    }

    m_prg->mreg.setRequest(m_speedL * q, m_speedR * q);
}

void rg::steer_normal::onEnter() {
    signalStart();

    m_left0 = m_prg->msr.tachoLeft;
    m_right0 = m_prg->msr.tachoRight;

    m_leftSP = m_left0 + m_angleL;
    m_rightSP = m_right0 + m_angleR;

    m_prg->mreg.start();
}

///////////////////////
// REGULATED TURNING //
///////////////////////

rg::turn_regulated::turn_regulated(rg::program &prg,
                                   float degreesCCW,
                                   int rotSpeed)
        : move_state(prg, "regulated rotation", false) {

    m_ccw = degreesCCW > 0;
    m_angleDiff = degreesCCW;
    m_speedAbs = rotSpeed;
    m_maxError = prg.mdata.rotRegMaxError;
}

void rg::turn_regulated::onEnter() {
    signalStart();

    m_angle0 = m_prg->odom.theta;
    m_angleSP = m_angle0 + m_angleDiff;

    m_prg->mreg.start();
}

void rg::turn_regulated::update() {
    float rotated = m_prg->odom.theta - m_angle0;
    float remaining = m_angleSP - m_prg->odom.theta;
    if (!m_ccw) {
        remaining *= -1;
        rotated *= -1;
    }
    if (remaining <= m_maxError) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }

    float fix = m_prg->rramp.rampQuotient(rotated, remaining, m_speedAbs) * m_speedAbs;

    m_prg->mreg.setRequest(m_ccw ? -fix : +fix,
                           m_ccw ? +fix : -fix);
}

void rg::turn_regulated::stallDetected() {
    // ignore
}

////////////////////////
// REGULATED STEERING //
////////////////////////

rg::steer_regulated::steer_regulated(
        rg::program &prg, float radius, float degreesCCW, bool forward, int speed, bool reactToTouch)
        : move_state(prg, "regulated steering", !forward) {

    float shorterS = (radius) / (radius + prg.mdata.trackRadius) * speed;
    float longerS = (radius + 2 * prg.mdata.trackRadius) / (radius + prg.mdata.trackRadius) * speed;
    m_angleDiff = forward ? +degreesCCW : -degreesCCW;

    bool wheelCCW = degreesCCW > 0;
    bool angleCCW = wheelCCW ^!forward;
    int bwdInv = inversion_sign(!forward);

    m_ccw = angleCCW;
    m_wheelCcw = wheelCCW;
    m_speedL = ftoi(wheelCCW ? shorterS : longerS) * bwdInv;
    m_speedR = ftoi(wheelCCW ? longerS : shorterS) * bwdInv;

    m_maxError = prg.mdata.steerRegMaxError;
    m_reactToTouch = reactToTouch;
}

void rg::steer_regulated::onEnter() {
    signalStart();

    m_angle0 = m_prg->odom.theta;
    m_angleSP = m_angle0 + m_angleDiff;

    m_prg->mreg.start();
}

void rg::steer_regulated::update() {
    float rotated = m_prg->odom.theta - m_angle0;
    float remaining = m_angleSP - m_prg->odom.theta;
    if (!m_ccw) {
        remaining *= -1;
        rotated *= -1;
    }
    if (remaining <= m_maxError) {
        m_prg->mreg.stop();
        finish(finish_reason::movement_finished);
        return;
    }

    float fix = m_prg->rramp.rampQuotient(rotated, remaining, std::max(std::abs(m_speedL), std::abs(m_speedR)));

    m_prg->mreg.setRequest(m_speedL * fix, m_speedR * fix);
}

void rg::steer_regulated::stallDetected() {
    // ignore
}

void rg::steer_regulated::touchDetected() {
    if (m_reactToTouch) {
        move_state::touchDetected();
    }
}


//////////////////
// CAGE UP/DOWN //
//////////////////

rg::cage_operation::cage_operation(program &prg, bool requestDown)
        : move_state(prg, requestDown ? "cage down" : "cage up", false),
          m_targetTacho(requestDown
                        ? prg.mech.getCageZero() + prg.mdata.cageTarget
                        : prg.mech.getCageZero()),
          m_speed(prg.mdata.cageSpeed) {}

void rg::cage_operation::update() {
    motor_state st = m_prg->dev.cage.getState();
    if (!is(st, motor_running) || is(st, motor_stalled)) {
        finish(finish_reason::movement_finished);
    }
}

void rg::cage_operation::onEnter() {
    signalStart();
    m_prg->dev.cage.setSpeedSP(m_speed);
    m_prg->dev.cage.setPositionSP(m_targetTacho);
    m_prg->dev.cage.doThe(motor_action::run_to_abs_pos);
}

void rg::cage_operation::stallDetected() {
    // ignore
}

void rg::cage_operation::touchDetected() {
    // ignore
}
