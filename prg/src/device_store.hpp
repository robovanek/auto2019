//
// Created by kuba on 19.3.19.
//

#ifndef ROBOGYMPL_DEVICE_STORE_HPP
#define ROBOGYMPL_DEVICE_STORE_HPP

#include <lego_ultrasonic.hpp>
#include <motor.hpp>
#include <lego_gyro.hpp>
#include <lego_touch.hpp>
#include <lego_color.hpp>
#include <Registry.hpp>
#include <settings.h>

namespace rg {
    class program;

    struct device_store {
        explicit device_store(program &prg);

        lego_ultrasonic sonic;
        lego_color color;
        lego_touch touch;
        lego_gyro gyro;

        motor left;
        motor right;
        motor cage;
    };
}


#endif //ROBOGYMPL_DEVICE_STORE_HPP
