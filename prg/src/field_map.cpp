//
// Created by kuba on 18.3.19.
//

#include "field_map.hpp"

std::array<rg::tile, 3> rg::all_tiles = {rg::tile::unknown, rg::tile::wall, rg::tile::free};
std::array<rg::orient, 4> rg::all_orients = {rg::orient::north, rg::orient::south, rg::orient::east, rg::orient::west};


rg::field_map::field_map() : m_array(width * height) {
    erase();
    drawInitial();
}

void rg::field_map::erase() {
    m_array.assign(width * height, tile::unknown);
}

void rg::field_map::drawInitial() {
    // start
    set(pos(4, 3), tile::wall);
    set(pos(5, 3), tile::wall);
    set(pos(6, 3), tile::wall);

    // finish
    set(pos(0, 0), tile::free);
    set(pos(0, 1), tile::free);
    set(pos(0, 2), tile::free);
    set(pos(1, 0), tile::wall);
    set(pos(1, 1), tile::wall);

    // garage
    set(pos(8, 5), tile::free);
    set(pos(7, 5), tile::free);
    set(pos(6, 5), tile::free);
    set(pos(8, 4), tile::wall);
    set(pos(7, 4), tile::wall);
}

rg::tile rg::field_map::get(const pos &where) const {
    if (isIndexValid(where)) {
        return deref(where);
    }
    return tile::wall;
}

void rg::field_map::set(const pos &where, rg::tile t) {
    if (isIndexValid(where)) {
        deref(where) = t;
    }
}

rg::pos rg::pos::advance(rg::orient dir, int amount) const {
    switch (dir) {
        case orient::north:
            return {m_x, m_y - amount};
        case orient::south:
            return {m_x, m_y + amount};
        case orient::east:
            return {m_x + amount, m_y};
        case orient::west:
            return {m_x - amount, m_y};
    }
}

rg::orient rg::pos::direction_to(const rg::pos &other) const {
    if (*this == other) {
        throw std::logic_error("pos::direction_to() called with target=source");
    }
    if (other.x() == this->x()) {
        if (other.y() > this->y()) {
            return orient::south;
        } else {
            return orient::north;
        }
    } else if (other.y() == this->y()) {
        if (other.x() > this->x()) {
            return orient::east;
        } else {
            return orient::west;
        }
    } else {
        throw std::logic_error("pos::direction_to() called with a target that is not reachable in any direction");
    }
}

rg::relative_position rg::compare_directions(rg::orient oldO, rg::orient newO) {
    switch (oldO) {
        case orient::north:
            switch (newO) {
                case orient::north:
                    return relative_position::new_same;
                case orient::south:
                    return relative_position::new_opposite;
                case orient::east:
                    return relative_position::new_to_right;
                case orient::west:
                    return relative_position::new_to_left;
            }
        case orient::south:
            switch (newO) {
                case orient::north:
                    return relative_position::new_opposite;
                case orient::south:
                    return relative_position::new_same;
                case orient::east:
                    return relative_position::new_to_left;
                case orient::west:
                    return relative_position::new_to_right;
            }
        case orient::east:
            switch (newO) {
                case orient::north:
                    return relative_position::new_to_left;
                case orient::south:
                    return relative_position::new_to_right;
                case orient::east:
                    return relative_position::new_same;
                case orient::west:
                    return relative_position::new_opposite;
            }
        case orient::west:
            switch (newO) {
                case orient::north:
                    return relative_position::new_to_right;
                case orient::south:
                    return relative_position::new_to_left;
                case orient::east:
                    return relative_position::new_opposite;
                case orient::west:
                    return relative_position::new_same;
            }
    }
}


rg::orient rg::rotate(rg::orient orig, rg::rotation rot) {
    switch (orig) {
        case orient::north:
            return rot == rotation::left ? orient::west : orient::east;
        case orient::south:
            return rot == rotation::left ? orient::east : orient::west;
        case orient::east:
            return rot == rotation::left ? orient::north : orient::south;
        case orient::west:
            return rot == rotation::left ? orient::south : orient::north;
    }
}

rg::orient rg::rotate(orient orig, rg::rotation rot, int amount) {
    amount = amount % 4;
    if (rot == rotation::left) {
        amount = -amount;
    } else if (rot == rotation::right) {
        amount = +amount;
    }

    if (amount == 2 || amount == -2) {
        switch (orig) {
            case orient::north:
                return orient::south;
            case orient::south:
                return orient::north;
            case orient::east:
                return orient::west;
            case orient::west:
                return orient::east;
        }
    } else if (amount == 1 || amount == -3) {
        switch (orig) {
            case orient::north:
                return orient::east;
            case orient::south:
                return orient::west;
            case orient::east:
                return orient::south;
            case orient::west:
                return orient::north;
        }
    } else if (amount == 3 || amount == -1) {
        switch (orig) {
            case orient::north:
                return orient::west;
            case orient::south:
                return orient::east;
            case orient::east:
                return orient::north;
            case orient::west:
                return orient::south;
        }
    } else /* if (amount == 0) */ {
        return orig;
    }
}
