//
// Created by kuba on 24.3.19.
//

#ifndef ROBOGYMPL_EXCEPTIONS_HPP
#define ROBOGYMPL_EXCEPTIONS_HPP

#include <exception>

namespace rg {
    [[noreturn]] [[gnu::noinline]]
    extern void exception_hook();
}

#endif //ROBOGYMPL_EXCEPTIONS_HPP
