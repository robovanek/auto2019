//
// Created by kuba on 19.3.19.
//

#include "measurements.hpp"
#include <program.hpp>

void rg::measurements::update() {
    sonicDistance = m_prg->dev.sonic.readDistance();
    redReflection = m_prg->dev.color.readReflection();
    gyroAngle = m_prg->dev.gyro.readAngle();
    touchPressed = m_prg->dev.touch.isPressed();
    tachoLeft = m_prg->dev.left.getCurrentPosition();
    tachoRight = m_prg->dev.right.getCurrentPosition();
    speedLeft = m_prg->dev.left.getCurrentSpeed();
    speedRight = m_prg->dev.right.getCurrentSpeed();
}

rg::measurements::measurements(program &prg)
        : task("measurement collection", prg) {}
