//
// Created by kuba on 19.3.19.
//

#include <motor.hpp>
#include <lego_gyro.hpp>
#include "odometry.hpp"
#include "program.hpp"

template<typename T>
T diff_advance(T newPos, T &oldPos) {
    T diff = newPos - oldPos;
    oldPos = newPos;
    return diff;
}

rg::odometry::odometry(program &prg)
        : task("odometry calculation", prg),
          m_wheelRadius(m_prg->cfg.get_float("wheel radius")),
          m_trackRadius(m_prg->cfg.get_float("track radius")),
          m_gyroAngleTrust(m_prg->cfg.get_float("gyro angle trust")),
          m_gyrodometry(m_prg->cfg.get_bool("use gyrodometry")),
          m_gyrodometry_threshold(m_prg->cfg.get_float("gyrodometry threshold")) {}

void rg::odometry::update() {
    int deltaL = diff_advance(m_prg->msr.tachoLeft, m_oldL);
    int deltaR = diff_advance(m_prg->msr.tachoRight, m_oldR);
    int gyroDeltaA = -diff_advance(m_prg->msr.gyroAngle, m_oldGyro);
    if (!m_hasOld) {
        m_hasOld = true;
        return;
    }
    float advancement;
    float motorRate;
    float motorDeltaA;

    // convert degrees angle to an arc length divided by two
    float coefV = (float) M_PI / 360.0f * m_wheelRadius;
    float coefW = m_wheelRadius / (m_trackRadius * 2.0f);

    // linear velocity / change in position
    velocity = coefV * (m_prg->msr.speedRight + m_prg->msr.speedLeft);
    advancement = coefV * (deltaR + deltaL);

    // angular velocity / change in angle
    motorRate = coefW * (m_prg->msr.speedRight - m_prg->msr.speedLeft);
    motorDeltaA = coefW * (deltaR - deltaL);

    // combine the innovations
    float innovation;
    if (m_gyrodometry) {
        if (fabsf(motorDeltaA - gyroDeltaA) > m_gyrodometry_threshold) {
            innovation = gyroDeltaA;
        } else {
            innovation = motorDeltaA;
        }
    } else {
        innovation = gyroDeltaA * m_gyroAngleTrust + motorDeltaA * (1 - m_gyroAngleTrust);
    }
    theta += innovation;
    omega = motorRate;

    // add the difference of tacho
    x -= sinf(theta * (float) M_PI / 180.0f) * advancement;
    y -= cosf(theta * (float) M_PI / 180.0f) * advancement;
}

rg::orient rg::odometry::theta_to_orient(float theta) {
    while (theta >= 360.0f) {
        theta -= 360.0f;
    }
    while (theta < 0.0f) {
        theta += 360.0f;
    }
    if (theta < 45.0f || theta >= 315.0f) {
        return orient::north;
    } else if (theta >= 45.0f && theta < 135.0f) {
        return orient::west;
    } else if (theta >= 135.0f && theta < 225.0f) {
        return orient::south;
    } else if (theta >= 225.0f && theta < 315.0f) {
        return orient::east;
    }
    return orient::north;
}

float rg::odometry::theta_from_orient(rg::orient where) {
    switch (where) {
        case orient::north:
            return 0.0f;
        case orient::south:
            return 180.0f;
        case orient::east:
            return 270.0f;
        case orient::west:
            return 90.0f;
    }
}

float rg::odometry::angle_to_nearest_theta(float angle) {
    float fulls = floorf(angle / 360.0f);
    float stepPart = angle - 360.0f * fulls;

    if (stepPart < 45.0f) {
        return 0.0f - stepPart;
    } else if (stepPart >= 315.0f) {
        return 360.0f - stepPart;
    } else if (stepPart >= 45.0f && stepPart < 135.0f) {
        return 90.0f - stepPart;
    } else if (stepPart >= 135.0f && stepPart < 225.0f) {
        return 180.0f - stepPart;
    } else if (stepPart >= 225.0f && stepPart < 315.0f) {
        return 270.0f - stepPart;
    } else {
        throw std::logic_error("angle_to_nearest() is totally broken");
    }
}

float rg::odometry::nearest_theta(float theta) {
    float fullSteps = floorf(theta / 360.0f);
    float stepPart = theta - fullSteps * 360.0f;

    return theta_from_orient(theta_to_orient(stepPart)) + fullSteps * 360.0f;
}

std::tuple<float, float> rg::odometry::coords_from_pos(const rg::pos &p) {
    return {(int) (p.x() * 280.0f + 140.0f), (int) (p.y() * 280.0f + 140.0f)};
}

rg::pos rg::odometry::coords_to_pos(float xF, float yF) {
    int x = (int) ((xF - 140.0f) / 280.0f);
    int y = (int) ((yF - 140.0f) / 280.0f);
    return {x, y};
}

std::tuple<float, float> rg::odometry::nearest_pos(const std::tuple<float, float> &pos) {
    return coords_from_pos(coords_to_pos(pos));
}

void rg::odometry::reset() {
    fromTuple(coords_from_pos(pos_start));
    velocity = 0.0f;
    theta = 0.0f;
    omega = 0.0f;
    m_hasOld = false;
}

std::tuple<float, float> rg::odometry::asTuple() const {
    return {x, y};
}

void rg::odometry::fromTuple(const std::tuple<float, float> &tuple) {
    x = std::get<0>(tuple);
    y = std::get<1>(tuple);
}
