//
// Created by kuba on 18.3.19.
//

#ifndef PRG_MAP_HPP
#define PRG_MAP_HPP

#include <vector>
#include <utility>
#include <functional>

namespace rg {
    enum class tile {
        unknown,
        wall,
        free
    };

    enum class orient {
        north,
        south,
        east,
        west
    };

    enum class rotation {
        left,
        right
    };

    enum class relative_position {
        new_same,
        new_to_left,
        new_to_right,
        new_opposite
    };

    extern std::array<tile, 3> all_tiles;
    extern std::array<orient, 4> all_orients;

    orient rotate(orient orig, rotation rot);

    orient rotate(orient orig, rotation rot, int amount);

    relative_position compare_directions(orient oldO, orient newO);

    class pos {
    public:

        constexpr pos() noexcept : m_x(0), m_y(0) {}

        constexpr pos(int x, int y) noexcept : m_x(x), m_y(y) {}

        int x() const noexcept {
            return m_x;
        }

        int y() const noexcept {
            return m_y;
        }

        bool operator==(const pos &other) const {
            return this->m_x == other.m_x && this->m_y == other.m_y;
        }

        bool operator!=(const pos &other) const {
            return !(*this == other);
        }

        pos advance(orient dir, int amount = 1) const;

        orient direction_to(const pos &other) const;

    private:
        int m_x;
        int m_y;
    };

    static constexpr pos pos_start(5, 3);
    static constexpr pos pos_garage(0, 0);
    static constexpr pos pos_garage_marker(0, 1);
    static constexpr pos pos_finish(8, 5);
    static constexpr pos pos_finish_marker(7, 5);


    class field_map {
    public:
        static constexpr int width = 9;
        static constexpr int height = 6;

        field_map();

        void erase();

        void drawInitial();

        tile get(const pos &where) const;

        void set(const pos &where, tile t);

        static bool isIndexValid(const pos &where) {
            return 0 <= where.x() && where.x() < width && 0 <= where.y() && where.y() < height;
        }

    private:
        static int getIndexOffset(const pos &where) {
            return where.y() * width + where.x();
        }

        tile deref(const pos &where) const {
            return m_array[getIndexOffset(where)];
        }

        tile &deref(const pos &where) {
            return m_array[getIndexOffset(where)];
        }

        std::vector<tile> m_array;
    };
}

namespace std {
    template<>
    struct hash<rg::pos> {
        size_t operator()(const rg::pos &pos) const {
            int value = pos.y() * rg::field_map::width + pos.x();
            return static_cast<size_t>(value);
        }
    };
}

#endif //PRG_MAP_HPP
