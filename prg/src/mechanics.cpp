//
// Created by kuba on 21.3.19.
//

#include "mechanics.hpp"
#include "program.hpp"

bool rg::is(rg::motor_state current, rg::motor_state check) {
    return (current & check) != 0;
}

/////////////////
// CONSTRUCTOR //
/////////////////

rg::mechanics::mechanics(program &prg)
        : task("mechanics", prg) {}

////////////
// STARTS //
////////////

void rg::mechanics::stop() {
    m_prg->mreg.stop();
}

void rg::mechanics::initialize() {
    int cageRampUpTime = m_prg->cfg.get_integer("cage rampup time");
    int cageRampDownTime = m_prg->cfg.get_integer("cage rampdown time");
    bool cageNormal = m_prg->cfg.get_bool("cage is normal polarity");

    m_prg->dev.left.setRampUpSP(m_prg->mdata.rampUpTime);
    m_prg->dev.left.setRampDownSP(m_prg->mdata.rampDownTime);
    m_prg->dev.left.setPolarity(true);
    m_prg->dev.left.setStopAction(stop_action::brake);
    m_prg->dev.right.setRampUpSP(m_prg->mdata.rampUpTime);
    m_prg->dev.right.setRampDownSP(m_prg->mdata.rampDownTime);
    m_prg->dev.right.setPolarity(true);
    m_prg->dev.right.setStopAction(stop_action::brake);
    m_prg->dev.cage.setRampUpSP(cageRampUpTime);
    m_prg->dev.cage.setRampDownSP(cageRampDownTime);
    m_prg->dev.cage.setPolarity(cageNormal);
    m_prg->dev.cage.setStopAction(stop_action::hold);
    m_cageZero = m_prg->dev.cage.getCurrentPosition();

    m_maxSpeed = std::min(m_prg->dev.left.getMaximumSpeed(), m_prg->dev.right.getMaximumSpeed());
}

void rg::mechanics::update() {
    if (m_jobs.empty()) {
        return;
    }

    move_state &op = *m_jobs.front();
    if (!op.hasStarted()) {
        op.onEnter();
    }

    // running, ramping, holding, overloaded, stalled
    motor_state stL = m_prg->dev.left.getState();
    motor_state stR = m_prg->dev.right.getState();

    if (is(stL, motor_stalled) or is(stR, motor_stalled)) {
        bool leftBug = is(stL, motor_running) and is(stL, motor_ramping) and is(stL, motor_stalled);
        bool rightBug = is(stR, motor_running) and is(stR, motor_ramping) and is(stR, motor_stalled);
        if (leftBug || rightBug) {
            // try to restart the move
            op.onEnter();
        } else {
            op.stallDetected();
        }
    }

    if (m_prg->msr.touchPressed) {
        op.touchDetected();
    }

    if (op.isActive()) {
        op.update();
    }

    if (!op.isActive()) {
        op.onExit();
        m_reason = op.whyEnded();
        m_jobs.pop();
    }
}

void rg::mechanics::pushMovePtr(std::unique_ptr<rg::move_state> move) {
    if (m_carriageMode) {
        auto op = make_unique<cage_operation>(*m_prg, move->needsCageDown());
        m_jobs.push(std::move(op));
    }
    m_jobs.push(std::move(move));
}

int rg::mechanics::getCageZero() {
    return m_cageZero;
}

///////////////////
// CONFIGURATION //
///////////////////

rg::move_data::move_data(robofat::settings &cfg)
        : wheelRadius(cfg.get_float("wheel radius")),
          trackRadius(cfg.get_float("track radius")),
          fwdSpeed(linearMoveDegrees(cfg.get_float("speed forward"))),
          bwdSpeed(linearMoveDegrees(cfg.get_float("speed backward"))),
          steerSpeed(linearMoveDegrees(cfg.get_float("speed steering"))),
          turnSpeed(linearMoveDegrees(cfg.get_float("speed turning"))),
          alignSpeed(linearMoveDegrees(cfg.get_float("speed align"))),
          cageSpeed(cfg.get_integer("cage speed")),
          cageTarget(cfg.get_integer("cage target angle")),
          rotRegMaxError(cfg.get_float("turning regulator maxerror")),
          rotRegSlowBand(cfg.get_float("turning regulator slowband")),
          rotRegP(cfg.get_float("turning regulator p")),
          rotRegulated(cfg.get_bool("regulated rotation")),
          steerRegulated(cfg.get_bool("regulated steering")),
          attachTime(cfg.get_integer("attach time")),
          steerRegSlowBand(cfg.get_float("steer regulator slowband")),
          steerRegP(cfg.get_float("steer regulator p")),
          steerRegMaxError(cfg.get_float("steer regulator maxerror")),
          fwdRegSonicMin(cfg.get_integer("fwd regulator sonic min")),
          fwdRegSonicMax(cfg.get_integer("fwd regulator sonic max")),
          fwdRegColorMin(cfg.get_integer("fwd regulator color min")),
          fwdRegColorMax(cfg.get_integer("fwd regulator color max")),
          fwdRegColorSqrt(cfg.get_bool("fwd regulator color sqrt")),
          fwdRegSonicSetpoint(cfg.get_integer("fwd regulator sonic sp")),
          fwdRegColorSetpoint(cfg.get_integer("fwd regulator color sp")),
          fwdRegRelMin(cfg.get_float("fwd regulator out relmin")),
          fwdRegRelMax(cfg.get_float("fwd regulator out relmax")),
          fwdRegColorPIDa(cfg.get_float("fwd regulator color kp"),
                          cfg.get_float("fwd regulator color ki"),
                          cfg.get_float("fwd regulator color kd"),
                          cfg.get_float("fwd regulator color i forget"),
                          cfg.get_float("fwd regulator color i max")),
          fwdRegSonicPIDa(cfg.get_float("fwd regulator sonic kp"),
                          cfg.get_float("fwd regulator sonic ki"),
                          cfg.get_float("fwd regulator sonic kd"),
                          cfg.get_float("fwd regulator sonic i forget"),
                          cfg.get_float("fwd regulator sonic i max")),
          fwdRegColorPIDb(cfg.get_float("fwd regulator color dfilt now")),
          fwdRegSonicPIDb(cfg.get_float("fwd regulator sonic dfilt now")),
          rampUpTime(0/*cfg.get_integer("rampup time")*/),
          rampDownTime(0/*cfg.get_integer("rampdown time")*/),
          rotRegRelMin(cfg.get_float("turning regulator relmin")),
          rotRegRelMax(cfg.get_float("turning regulator relmin")),
          steerRegRelMin(cfg.get_float("steer regulator relmin")),
          steerRegRelMax(cfg.get_float("steer regulator relmax")),
          fwdRegRelOutMin(cfg.get_float("fwd regulator relmin")),
          fwdRegRelOutP(cfg.get_float("fwd regulator p")),
          fwdRegRelOutSlowBand(cfg.get_float("fwd regulator slowband")),
          attachSpeed(linearMoveDegrees(cfg.get_float("speed attach"))) {}

int rg::move_data::linearMoveDegrees(float mm) {
    return (int) roundf(mm / (2.0f * PI_F * wheelRadius) * 360.0f);
}

float rg::move_data::moveMillimeters(float degs) {
    return degs / 360.0f * 2.0f * PI_F * wheelRadius;
}
