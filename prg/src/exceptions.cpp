//
// Created by kuba on 24.3.19.
//

#include <exceptions.hpp>
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <backtrace.h>
#include <cstring>
#include <cxxabi.h>
#include <sstream>

struct btinfo {
    btinfo() : frame(1) {

    }

    int frame;
};

void backtrace_error(void *data, const char *msg, int errnum) {
    std::cerr << "Backtrace error: " << msg;
    if (errnum != -1) {
        std::cerr << "[errno=" << errnum << " ~ '" << strerror(errnum) << "']";
    }
    std::cerr << std::endl << std::flush;
}

std::string demangle(const char *function, bool &cpp) {
    int status = 0;
    char *demangled = abi::__cxa_demangle(function, nullptr, nullptr, &status);

    std::ostringstream ostr;
    cpp = false;
    switch (status) {
        case 0:
            ostr << demangled;
            cpp = true;
            break;
        case -1:
            ostr << "[" << function << "/memory alloc error]";
            break;
        case -2:
            ostr << function << "(...)";
            break;
        case -3:
            ostr << "[" << function << "/invalid argument]";
            break;
        default:
            ostr << "[" << function << "/unknown demangle error]";
            break;
    }
    if (demangled != nullptr) {
        free(demangled);
    }
    return ostr.str();
}

int backtrace_full_handler(void *data, uintptr_t pc,
                           const char *filename, int lineno,
                           const char *function) {
    btinfo &info = *reinterpret_cast<btinfo *>(data);

    std::cerr << "* frame #" << info.frame << ": " << std::endl;

    bool cpp = false;
    std::cerr << "  - name:    ";
    if (function != nullptr) {
        std::cerr << demangle(function, cpp);
    } else {
        std::cerr << "[name unknown]";
    }
    std::cerr << std::endl;
    std::cerr << "  - linkage: " << (cpp ? "C++" : "C") << std::endl;
    std::cerr << "  - address: " << reinterpret_cast<const void *>(pc) << std::endl;

    std::cerr << "  - source:  ";
    if (filename != nullptr) {
        std::cerr << filename;
    } else {
        std::cerr << "unknown";
    }

    if (lineno != 0) {
        std::cerr << ":" << lineno;
    }
    std::cerr << std::endl << std::flush;
    info.frame++;
    if (function != nullptr && std::strcmp(function, "main") == 0) {
        return 1;
    } else {
        return 0;
    }
}

void rg::exception_hook() {
    std::exception_ptr eptr = std::current_exception();
    if (eptr) {
        bool cpp = false;
        std::cerr << "Exception std::terminate()." << std::endl;
        std::cerr << "- type:    " << demangle(eptr.__cxa_exception_type()->name(), cpp) << std::endl;
        try {
            std::rethrow_exception(eptr);
        } catch (const std::exception &e) {
            std::cerr << "- message: " << e.what() << std::endl;
        } catch (...) {
            std::cerr << "- message: <unknown>" << std::endl;
        }
    } else {
        std::cerr << "Non-exception std::terminate()." << std::endl;
    }

    std::cerr << "Backtrace:" << std::endl;

    btinfo info;
    backtrace_state *state = backtrace_create_state(nullptr, 0, &backtrace_error, nullptr);
    backtrace_full(state, 0, &backtrace_full_handler, &backtrace_error, &info);
    std::cerr << std::flush;

    std::raise(SIGINT);

    exit(1);
}
