//
// Created by kuba on 25.3.19.
//

#include "linear_ramp.hpp"
#include <program.hpp>


rg::linear_ramp::linear_ramp(rg::program &prg)
        : m_prg(&prg),
          m_slowDegs(prg.cfg.get_float("lin slow degs")),
          m_rampUpDegs(prg.cfg.get_float("lin rampup degs")),
          m_rampDownDegs(prg.cfg.get_float("lin rampdown degs")),
          m_rampMin(prg.mdata.linearMoveDegrees(prg.cfg.get_float("lin ramp min mmps"))) {}

float rg::linear_ramp::rampDown(float remaining) {
    return (remaining - m_slowDegs) * (m_prg->mech.getMaxSpeed() / m_rampDownDegs);
}

float rg::linear_ramp::rampUp(float travelled) {
    return travelled * (m_prg->mech.getMaxSpeed() / m_rampUpDegs);
}

float rg::linear_ramp::rampQuotient(float travelled, float remaining, float speed) {
    float a = rampUp(travelled);
    float b = speed;
    float c = rampDown(remaining);
    return std::max(std::min(std::min(a, b), c), m_rampMin) / speed;
}

float rg::linear_ramp::travelled(int l0, int r0) {
    float a = m_prg->msr.tachoRight - r0;
    float b = m_prg->msr.tachoLeft - l0;

    return (fabsf(a) + fabsf(b)) / 2.0f;
}

float rg::linear_ramp::remaining(int lSP, int rSP) {
    float a = rSP - m_prg->msr.tachoRight;
    float b = lSP - m_prg->msr.tachoLeft;

    return (fabsf(a) + fabsf(b)) / 2.0f;
}
