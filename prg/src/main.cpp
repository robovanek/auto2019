#include <iostream>
#include "program.hpp"
#include "exceptions.hpp"
#include <csignal>

const char *default_config_path = "/home/robot/etc/robogympl.conf";

int main(int argc, char **argv) {
    std::set_terminate(&rg::exception_hook);
    std::string confpath = default_config_path;
    if (argc == 2) {
        confpath = argv[1];
    }

    robofat::display lcd;

    std::cerr << "Config path: " << confpath << std::endl << std::flush;
    std::cerr << "Initializing program..." << std::endl << std::flush;
    std::unique_ptr<rg::program> pProgram(new rg::program(confpath, nullptr));

    long select = lcd.show_menu("Calibrate gyro:", {"PROCEED", "CANCEL"});
    if (select == -1 || select == 1) {
        return 0;
    }

    std::cerr << "Calibrating gyro..." << std::endl << std::flush;
    pProgram->dev.gyro.calibrate();

    select = lcd.show_menu("Start round:", {"PROCEED", "CANCEL"});
    if (select == -1 || select == 1) {
        return 0;
    }
    std::cerr << "Go!" << std::endl << std::flush;
    pProgram->plan.enter_start();
    //pProgram->plan.enter_debug();
    pProgram->sched.enterLoop();
    std::cerr << "Exit! " << std::endl << std::flush;
}
