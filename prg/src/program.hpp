//
// Created by kuba on 18.3.19.
//

#ifndef PRG_GLOBAL_HPP
#define PRG_GLOBAL_HPP

#include <memory>
#include <device_store.hpp>
#include <measurements.hpp>
#include <odometry.hpp>
#include <scheduler.hpp>
#include <Connection.h>
#include <settings.h>
#include <planner.hpp>
#include <field_map.hpp>
#include <device_store.hpp>
#include <path_finder.hpp>
#include <display.h>
#include <linear_ramp.hpp>
#include "mechanics.hpp"
#include "environment.hpp"
#include "moves.hpp"
#include "rawreg.hpp"
#include "angular_ramp.hpp"

namespace rg {
    struct program {
        explicit program(const std::string &conf_path, std::shared_ptr<robofat::display> lcd);

        ~program();

        robofat::settings cfg;
        std::shared_ptr<robofat::display> lcd;
        scheduler sched;
        idle_task idle;
        ev3udev::udev_handle udev;
        ev3udev::Registry reg;
        ev3udev::Connection conn;
        device_store dev;
        measurements msr;
        odometry odom;
        planner plan;
        field_map map;
        path_finder find;
        move_data mdata;
        linear_ramp lramp;
        angular_ramp rramp;
        rawreg mreg;
        mechanics mech;
        environment env;
    };
}

#endif //PRG_GLOBAL_HPP
