//
// Created by kuba on 18.3.19.
//

#ifndef PRG_PLANNER_HPP
#define PRG_PLANNER_HPP

#include <settings.h>
#include <deque>
#include "scheduler.hpp"
#include "field_map.hpp"

namespace rg {
    class program;

    class planner : public task {
    private:
        enum class upper_mode {
            travel_to_garage,
            attach_carriage,
            travel_to_finish,
            debug
        };

        enum class travel_step {
            travel_to_msr_point,
            front_touch_backoff,
            travel_to_steer_finish
        };

        enum class attach_step {
            //go_to_center,
            turn_to_wall,
            /*            align_to_wall,            align_backoff,*/
            turn_back,
            travel_back,
            travel_to_msr_point
        };

        static constexpr attach_step attach_first = attach_step::turn_to_wall;

    public:
        explicit planner(rg::program &prg);

        void update() override;

        void enter_start();

        void enter_debug();

    private:
        void travel_loop();

        void attach_loop();

        void debug_loop();

        void runDecision();

        void enter_attach(attach_step step = attach_first);

        void enter_travel(travel_step step);

        void enter_goto_goal();

        float relativeAngleTo(rg::rotation rot);

        upper_mode m_master;
        travel_step m_travel;
        attach_step m_attach;
        float m_backoffLength;
        float m_attachFrontTravel;
        float m_attachY;
        float m_startDistance;
        float m_garageToCenterDistance;
        float m_cell2cellDistance;
        float m_steeringRadius;
        float m_steeringFinishDistance;
        float m_touchBackTravel;
        int m_freeWeight;
        int m_unknownWeight;
        bool m_absoluteOdometry;
        bool m_wallFolowing;

        float m_attachInRadius;
        float m_attachOutRadius;
        pos m_goal;

        void planBackward(const pos &first, const pos &second, std::deque<pos> &rest);
    };
}

#endif //PRG_PLANNER_HPP
