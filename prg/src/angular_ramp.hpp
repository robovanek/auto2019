//
// Created by kuba on 25.3.19.
//

#ifndef ROBOGYMPL_ANGULAR_RAMP_HPP
#define ROBOGYMPL_ANGULAR_RAMP_HPP

namespace rg {
    class program;

    class angular_ramp {
    public:
        explicit angular_ramp(program &prg);

        float rampQuotient(float rotated, float remaining, float speed);

    private:

        float rampUp(float rotated);

        float rampDown(float remaining);

        program *m_prg;
        float m_rampMin;
        float m_rampUpAng;
        float m_rampDownAng;
        float m_slowAng;
    };
}


#endif //ROBOGYMPL_ANGULAR_RAMP_HPP
