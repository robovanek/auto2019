#include <utility>

//
// Created by kuba on 18.3.19.
//

#include "program.hpp"

rg::program::program(const std::string &conf_path, std::shared_ptr<robofat::display> lcd)
        : cfg(conf_path),
          lcd(std::move(lcd)),
          sched(),
          idle(*this, (long) cfg.get_integer("loop msec")),
          udev(ev3udev::make_udev()),
          reg(udev),
          conn(*this, udev, reg),
          dev(*this),
          msr(*this),
          odom(*this),
          plan(*this),
          map(),
          find(map),
          mdata(this->cfg),
          lramp(*this),
          rramp(*this),
          mreg(*this),
          mech(*this),
          env(*this) {
    sched.append(conn);
    sched.append(msr);
    sched.append(odom);
    sched.append(plan);
    sched.append(env);
    sched.append(mech);
    sched.append(mreg);
    sched.append(idle);
    mech.initialize();
}

rg::program::~program() {
    dev.right.doThe(motor_action::reset);
    dev.left.doThe(motor_action::reset);
    dev.cage.doThe(motor_action::reset);
}
