//
// Created by kuba on 21.3.19.
//

#ifndef ROBOGYMPL_MECHANICS_HPP
#define ROBOGYMPL_MECHANICS_HPP

#include <settings.h>
#include "scheduler.hpp"
#include "device_store.hpp"
#include <pid.h>
#include <queue>

namespace rg {

    extern bool is(rg::motor_state current, rg::motor_state check);

    enum class finish_reason {
        touch_pressed,
        wheel_stall,
        movement_finished,
        forced_stop
    };

    struct move_data {
        explicit move_data(robofat::settings &cfg);

        int linearMoveDegrees(float mm);

        float moveMillimeters(float degs);

        float trackRadius;
        float wheelRadius;
        int fwdSpeed;
        int bwdSpeed;
        int steerSpeed;
        int turnSpeed;
        int alignSpeed;
        int cageSpeed;
        int cageTarget;

        float rotRegMaxError;
        float rotRegSlowBand;
        float rotRegP;

        bool rotRegulated;
        bool steerRegulated;
        int attachTime;

        int rampUpTime;
        int rampDownTime;

        float steerRegSlowBand;
        float steerRegP;
        float steerRegMaxError;

        int fwdRegSonicMin;
        int fwdRegSonicMax;
        int fwdRegColorMin;
        int fwdRegColorMax;
        bool fwdRegColorSqrt;
        int fwdRegSonicSetpoint;
        int fwdRegColorSetpoint;
        robofat::pid_consts<float> fwdRegColorPIDa;
        robofat::pid_consts<float> fwdRegSonicPIDa;
        robofat::pid_filter<float> fwdRegColorPIDb;
        robofat::pid_filter<float> fwdRegSonicPIDb;
        float fwdRegRelMax;
        float fwdRegRelMin;

        float rotRegRelMin;
        float rotRegRelMax;
        float steerRegRelMin;
        float steerRegRelMax;
        float fwdRegRelOutMin;
        float fwdRegRelBaseMin;
        float fwdRegRelOutP;
        float fwdRegRelOutSlowBand;
        float attachSpeed;
    };

    class move_state;

    class mechanics : public task {
    public:
        explicit mechanics(program &prg);

        void update() override;

        void stop();

        // +- DONE
        void initialize();

        finish_reason getFinishReason() const {
            return m_reason;
        }

        bool isFinished() const {
            return m_jobs.empty();
        }

        void setCarriageMode(bool rly) {
            m_carriageMode = rly;
        }

        int getCageZero();

        void pushMovePtr(std::unique_ptr<move_state> move);

        int getMaxSpeed() const {
            return m_maxSpeed;
        }

        template<typename T, typename... Args>
        void pushMove(Args &&...args) {
            return pushMovePtr(std::unique_ptr<T>(new T(*m_prg, std::forward<Args>(args)...)));
        }

    private:
        finish_reason m_reason = finish_reason::movement_finished;

        int m_cageZero = 0;
        int m_maxSpeed;

        bool m_carriageMode = false;

        std::queue<std::unique_ptr<move_state>> m_jobs;
    };
}


#endif //ROBOGYMPL_MECHANICS_HPP
