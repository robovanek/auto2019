//
// Created by kuba on 19.3.19.
//

#include "path_finder.hpp"
#include <unordered_map>
#include <algorithm>
#include <queue>

struct node_comparer;

struct node {
    typedef std::priority_queue<node *, std::vector<node *>, node_comparer> pq;
    typedef std::unordered_map<rg::pos, node> map;

    node(const rg::pos &where, rg::tile type) : coord(where), type(type) {}

    node *previous = nullptr;
    int distance = INT32_MAX;
    rg::pos coord;
    rg::tile type;
    bool visited = false;

    bool operator==(const node &other) const {
        return this->coord == other.coord;
    }

    bool operator!=(const node &other) const {
        return !(this->coord == other.coord);
    }
};

struct node_comparer {
    bool operator()(const node *a, const node *b) const {
        return a->distance > b->distance;
    }
};

node::map generate_nodes(const rg::field_map &map) {
    node::map result = {};

    for (int y = 0; y < rg::field_map::height; y++) {
        for (int x = 0; x < rg::field_map::width; x++) {
            rg::pos p(x, y);
            rg::tile t = map.get(p);
            if (t == rg::tile::free || t == rg::tile::unknown) {
                result.emplace(p, node(p, t));
            }
        }
    }

    return result;
}

std::deque<rg::pos> backtrack(node *end) {
    std::deque<rg::pos> result;
    if (end->distance == INT32_MAX) {
        return result;
    }

    while (end != nullptr) {
        result.push_front(end->coord);
        end = end->previous;
    }
    return result;
}

int getNewDistance(node *base, node *next, int freeWeight, int unknownWeight) {
    int new_distance = base->distance;
    if (next->type == rg::tile::free) {
        new_distance += freeWeight;
    } else if (next->type == rg::tile::unknown) {
        new_distance += unknownWeight;
    }
    return new_distance;
}

std::deque<rg::pos> rg::path_finder::find_shortest(const rg::pos &start, const rg::pos &goal) {
    node::map map = generate_nodes(*m_map);

    node &startNode = map.at(start);
    startNode.distance = 0;

    node::pq open;
    open.push(&startNode);

    while (!open.empty()) {
        node *base = open.top();
        open.pop();

        if (!base->visited) {
            base->visited = true;
        } else {
            continue;
        }

        for (orient dir : all_orients) {
            auto &&adj = map.find(base->coord.advance(dir));
            if (adj == map.end()) {
                continue;
            }

            node *next = &(adj->second);

            int new_distance = getNewDistance(base, next, m_freeWeight, m_unknownWeight);

            if (next->distance > new_distance) {
                next->distance = new_distance;
                next->previous = base;
                open.push(next);
            }
        }

    }

    return backtrack(&map.at(goal));
}

rg::path_finder::path_finder(field_map &map) : m_map(&map) {}
