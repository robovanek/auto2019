//
// Created by kuba on 19.3.19.
//

#ifndef ROBOGYMPL_PATH_FINDER_HPP
#define ROBOGYMPL_PATH_FINDER_HPP

#include <deque>
#include <memory>
#include "field_map.hpp"

namespace rg {

    class path_finder {
    public:
        explicit path_finder(field_map &map);

        std::deque<pos> find_shortest(const pos &start, const pos &goal);

        void set_weights(int free, int unknown) {
            m_unknownWeight = unknown;
            m_freeWeight = free;
        }

    private:
        int m_freeWeight = 1;
        int m_unknownWeight = 2;
        field_map *m_map;
    };
}


#endif //ROBOGYMPL_PATH_FINDER_HPP
