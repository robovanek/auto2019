//
// Created by kuba on 25.3.19.
//

#ifndef ROBOGYMPL_MOVES_HPP
#define ROBOGYMPL_MOVES_HPP

#include <string>
#include <mechanics.hpp>
#include <robofat.h>

namespace rg {

    class program;

    class move_state {
    public:
        move_state(program &prg, const std::string &name, bool needsCage);

        virtual void update() = 0;

        virtual void onEnter() = 0;

        virtual void onExit();

        virtual void stallDetected();

        virtual void touchDetected();

        bool needsCageDown();

        bool hasStarted();

        std::string name() const {
            return m_name;
        }

        bool isActive() const {
            return m_continue;
        }

        finish_reason whyEnded() const {
            return m_exitReason;
        }

    protected:
        void signalStart();

        void finish(finish_reason why) {
            m_continue = false;
            m_exitReason = why;
        }

        timestamp m_last;

        float timeCalc();

        program *m_prg;
        std::string m_name;
        finish_reason m_exitReason;
        bool m_continue;
        bool m_needsCage;
        bool m_started;
    };

    class line_move : public move_state {
    public:
        line_move(program &prg, float mm);

        line_move(program &prg, float mm, bool forward, int speed, bool reactToTouch);

        void update() override;

        void onEnter() override;

        void touchDetected() override;

    private:
        int m_degrees = 0;
        int m_speed = 0;
        bool m_forward;
        bool m_touch;
        bool m_infinite;
        int m_left0;
        int m_right0;
        int m_leftSP;
        int m_rightSP;
    };

    class line_move_timed : public line_move {
    public:
        line_move_timed(program &prg, int msecs, bool forward, int speed);
    };

    class forward_wall_move : public move_state {
    public:
        forward_wall_move(program &prg, float mm);

        forward_wall_move(program &prg, float mm, float speed);

        void update() override;

        void onEnter() override;

        void onExit() override;

        void stallDetected() override;

        void touchDetected() override;

    private:
        float regulateColor(float elapsed);
        float regulateSonic(float elapsed);

        std::list<float> m_sonicHistory;

        void addSonicMeasurement(float msr);
        float getSonicAverage();

        robofat::pid m_sonicPID;
        robofat::pid m_colorPID;
        int m_colorMin;
        int m_colorMax;
        int m_sonicMin;
        int m_sonicMax;
        int m_sonicSP;
        int m_colorSP;
        int m_degrees = 0;
        int m_speed = 0;
        bool m_colorSqrt;
        float m_relativeMax;
        float m_relativeMin;
        int m_leftSP;
        int m_rightSP;
        int m_left0;
        int m_right0;

        float m_sonicBug1MaxFix;
        int m_sonicHistoryLength;
        float m_sonicBug2Tolerance;
    };


    class turn_normal : public move_state {
    public:
        turn_normal(program &prg, float degreesCCW, int rotSpeed);

        void update() override;

        void onEnter() override;

    private:
        bool m_ccw;
        int m_angleAbs;
        int m_speedAbs;

        int m_right0;
        int m_left0;
        int m_rightSP;
        int m_leftSP;
    };

    class steer_normal : public move_state {
    public:
        steer_normal(program &prg, float radius, float degreesCCW, bool forward, int speed);

        void update() override;

        void onEnter() override;

    private:
        bool m_forward;
        bool m_ccw;

        int m_angleL;
        int m_angleR;
        int m_speedL;
        int m_speedR;

        int m_right0;
        int m_left0;
        int m_rightSP;
        int m_leftSP;
    };

    class turn_regulated : public move_state {
    public:
        turn_regulated(program &prg, float degreesCCW, int rotSpeed);

        void update() override;

        void onEnter() override;

        void stallDetected() override;

    private:
        bool m_ccw;
        float m_angleDiff;
        int m_speedAbs;
        float m_maxError;

        float m_angle0;
        float m_angleSP;
    };

    class steer_regulated : public move_state {
    public:
        steer_regulated(program &prg, float radius, float degreesCCW, bool forward, int speed, bool reactToTouch = true);

        void update() override;

        void onEnter() override;

        void stallDetected() override;

        void touchDetected() override;

    private:
        float m_angle0;
        float m_angleSP;
        float m_angleDiff;
        int m_speedL;
        int m_speedR;
        bool m_ccw;
        bool m_wheelCcw;
        float m_maxError;
        bool m_reactToTouch;
    };

    class cage_operation : public move_state {
    public:
        cage_operation(program &prg, bool requestDown);

        void update() override;

        void onEnter() override;

        void stallDetected() override;

        void touchDetected() override;

    private:
        int m_targetTacho;
        int m_speed;
    };

}


#endif //ROBOGYMPL_MOVES_HPP
