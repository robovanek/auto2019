//
// Created by kuba on 18.3.19.
//

#include <util.h>
#include "planner.hpp"
#include "program.hpp"

rg::planner::planner(rg::program &prg)
        : task("planner", prg),
          m_master(upper_mode::travel_to_garage),
          m_travel(travel_step::travel_to_msr_point),
          m_attach(attach_step::turn_to_wall),
          m_absoluteOdometry(m_prg->cfg.get_bool("absolute rotation")),
          m_backoffLength(m_prg->cfg.get_float("garage backoff distance")),
          m_attachFrontTravel(m_prg->cfg.get_float("travel after attach")),
          m_attachY(m_prg->cfg.get_float("after attach y")),
          m_startDistance(m_prg->cfg.get_float("travel on start")),
          m_garageToCenterDistance(m_prg->cfg.get_float("garage to center distance")),
          m_cell2cellDistance(m_prg->cfg.get_float("cell to cell distance")),
          m_steeringRadius(m_prg->cfg.get_float("steering radius")),
          m_steeringFinishDistance(m_prg->cfg.get_float("steering finish distance")),
          m_wallFolowing(m_prg->cfg.get_bool("wall following")),
          m_touchBackTravel(m_prg->cfg.get_float("regular touch backoff")),
          m_freeWeight(m_prg->cfg.get_integer("weight free")),
          m_unknownWeight(m_prg->cfg.get_integer("weight unknown")),
          m_attachInRadius(m_prg->cfg.get_float("attach in radius")),
          m_attachOutRadius(m_prg->cfg.get_float("attach out radius")) {}

void rg::planner::update() {
    switch (m_master) {
        case upper_mode::travel_to_garage:
        case upper_mode::travel_to_finish:
            return travel_loop();
        case upper_mode::attach_carriage:
            return attach_loop();
        case upper_mode::debug:
            return debug_loop();
    }
}

void rg::planner::enter_start() {
    std::cerr << "[planner] start" << std::endl << std::flush;
    m_prg->odom.reset();
    m_master = upper_mode::travel_to_garage;
    m_travel = travel_step::travel_to_msr_point;
    m_goal = pos_garage_marker;
    if (m_wallFolowing) {
        m_prg->mech.pushMove<forward_wall_move>(m_startDistance);
    } else {
        m_prg->mech.pushMove<line_move>(m_startDistance);
    }
    m_prg->env.advanceForward();
}

void rg::planner::enter_travel(travel_step step) {
    m_travel = step;

    switch (step) {
        case travel_step::travel_to_msr_point:
            std::cerr << "[planner] travel::travel_to_msr_point" << std::endl << std::flush;
            // everything should be done by decision
            break;
        case travel_step::front_touch_backoff:
            std::cerr << "[planner] travel::front_touch_backoff" << std::endl << std::flush;
            break;
        case travel_step::travel_to_steer_finish:
            std::cerr << "[planner] travel::travel_to_steer_finish" << std::endl << std::flush;
            break;
    }
}

void rg::planner::travel_loop() {
    switch (m_travel) {
        case travel_step::travel_to_msr_point:
            if (m_prg->mech.isFinished()) {
                switch (m_prg->mech.getFinishReason()) {
                    case finish_reason::touch_pressed:
                        m_prg->mech.pushMove<line_move>(m_touchBackTravel, false, m_prg->mdata.alignSpeed, false);
                        m_prg->env.advanceBackward();
                        m_prg->env.writeWallHit();
                        enter_travel(travel_step::front_touch_backoff);
                        m_prg->odom.theta = odometry::nearest_theta(m_prg->odom.theta);
                        break;
                    case finish_reason::wheel_stall: {
                        // fixme a problem
                        float angle = odometry::angle_to_nearest_theta(m_prg->odom.theta);
                        m_prg->mech.pushMove<line_move>(m_backoffLength, false, m_prg->mdata.alignSpeed, false);
                        m_prg->mech.pushMove<turn_regulated>(angle, m_prg->mdata.turnSpeed);
                        enter_travel(travel_step::front_touch_backoff);
                        break;
                    }
                    case finish_reason::movement_finished:
                        m_prg->env.writeSides();
                        m_prg->map.set(m_prg->env.position, tile::free);
                        runDecision();
                        break;
                    case finish_reason::forced_stop:
                        // fixme a problem
                        break;
                }
            }
            break;
        case travel_step::front_touch_backoff:
            if (m_prg->mech.isFinished()) {
                runDecision();
            }
            break;
        case travel_step::travel_to_steer_finish:
            if (m_prg->mech.isFinished()) {
                if (m_steeringFinishDistance != 0.0f) {
                    m_prg->mech.pushMove<line_move>(m_steeringFinishDistance);
                    enter_travel(travel_step::travel_to_msr_point);
                } else {
                    switch (m_prg->mech.getFinishReason()) {
                        case finish_reason::touch_pressed:
                            m_prg->mech.pushMove<line_move>(m_touchBackTravel, false, m_prg->mdata.alignSpeed, false);
                            m_prg->env.advanceBackward();
                            m_prg->env.writeWallHit();
                            enter_travel(travel_step::front_touch_backoff);
                            m_prg->odom.theta = odometry::nearest_theta(m_prg->odom.theta);
                            break;
                        case finish_reason::wheel_stall: {
                            // fixme a problem
                            float angle = odometry::angle_to_nearest_theta(m_prg->odom.theta);
                            m_prg->mech.pushMove<line_move>(m_backoffLength, false, m_prg->mdata.alignSpeed, false);
                            m_prg->mech.pushMove<turn_regulated>(angle, m_prg->mdata.turnSpeed);
                            enter_travel(travel_step::front_touch_backoff);
                            break;
                        }
                        case finish_reason::movement_finished:
                            m_prg->env.writeSides();
                            m_prg->map.set(m_prg->env.position, tile::free);
                            runDecision();
                            break;
                        case finish_reason::forced_stop:
                            // fixme a problem
                            break;
                    }
                }
            }
            break;
    }
}

void rg::planner::enter_attach(attach_step step) {
    m_master = upper_mode::attach_carriage;
    m_attach = step;

    switch (step) {
        /*
        case attach_step::go_to_center:
            std::cerr << "[planner] attach::go_to_center" << std::endl << std::flush;
            m_prg->mech.pushMove<line_move>(m_garageToCenterDistance);
            break;
            */
        case attach_step::turn_to_wall:
            std::cerr << "[planner] attach::turn_to_wall" << std::endl << std::flush;
            m_prg->env.rotate(rotation::left);
            m_prg->env.position = pos_garage_marker;
            /*
            m_prg->mech.pushMove<turn_regulated>(relativeAngleTo(rotation::left), m_prg->mdata.turnSpeed);
            */
            m_prg->mech.pushMove<steer_regulated>(m_attachInRadius, relativeAngleTo(rotation::left), true,
                                                  m_prg->mdata.alignSpeed);
            m_prg->mech.pushMove<line_move>(INFINITY, true, m_prg->mdata.alignSpeed, true);
            break;
            /*
        case attach_step::align_to_wall:
            std::cerr << "[planner] attach::align_to_wall" << std::endl << std::flush;
            m_prg->mech.pushMove<line_move>(INFINITY, true, m_prg->mdata.alignSpeed, true);
            break;
        case attach_step::align_backoff:
            std::cerr << "[planner] attach::align_backoff" << std::endl << std::flush;
            m_prg->odom.theta = odometry::nearest_theta(m_prg->odom.theta);
            m_prg->mech.pushMove<line_move>(m_backoffLength, false, m_prg->mdata.alignSpeed, false);
            break;*/
        case attach_step::turn_back:
            std::cerr << "[planner] attach::turn_back" << std::endl << std::flush;
            m_prg->env.rotate(rotation::left);
            m_prg->env.position = pos_garage_marker;
            m_prg->odom.theta = odometry::nearest_theta(m_prg->odom.theta);
            m_prg->odom.fromTuple(odometry::coords_from_pos(pos_garage_marker));
            /*
            m_prg->mech.pushMove<turn_regulated>(relativeAngleTo(rotation::left), m_prg->mdata.turnSpeed);
             */
            m_prg->mech.pushMove<steer_regulated>(m_attachOutRadius, relativeAngleTo(rotation::right), false,
                                                  m_prg->mdata.alignSpeed, false);
            break;
        case attach_step::travel_back:
            m_prg->env.position = pos_garage_marker;
            std::cerr << "[planner] attach::travel_bactravel after attachk" << std::endl << std::flush;
            m_prg->mech.pushMove<line_move_timed>(m_prg->mdata.attachTime, false, m_prg->mdata.attachSpeed);
            break;
        case attach_step::travel_to_msr_point:
            m_prg->env.position = pos_garage_marker;
            std::cerr << "[planner] attach::travel_to_msr_point" << std::endl << std::flush;
            m_prg->odom.y = m_attachY;
            if (m_wallFolowing) {
                m_prg->mech.pushMove<forward_wall_move>(m_attachFrontTravel, m_prg->mdata.alignSpeed);
            } else {
                m_prg->mech.pushMove<line_move>(m_attachFrontTravel, true, m_prg->mdata.alignSpeed, true);
            }
            break;
    }
}

void rg::planner::attach_loop() {
    switch (m_attach) {
        /*
        case attach_step::go_to_center:
            if (m_prg->mech.isFinished()) {
                //enter_attach(attach_step::turn_to_wall);
                enter_attach(attach_step::turn_back);
            }
            break;*/
        case attach_step::turn_to_wall:
            m_prg->env.position = pos_garage_marker;
            if (m_prg->mech.isFinished()) {
                //enter_attach(attach_step::align_to_wall);
                enter_attach(attach_step::turn_back);
            }
            break;
            /*
        case attach_step::align_to_wall:
            if (m_prg->mech.isFinished()) {
                enter_attach(attach_step::align_backoff);
            }
            break;
        case attach_step::align_backoff:
            if (m_prg->mech.isFinished()) {
                enter_attach(attach_step::turn_back);
            }
            break;
             */
        case attach_step::turn_back:
            m_prg->env.position = pos_garage_marker;
            if (m_prg->mech.isFinished()) {
                enter_attach(attach_step::travel_back);
            }
            break;
        case attach_step::travel_back:
            m_prg->env.position = pos_garage_marker;
            if (m_prg->mech.isFinished()) {
                enter_attach(attach_step::travel_to_msr_point);
            }
            break;
        case attach_step::travel_to_msr_point:
            m_prg->env.direction = orient::south;
            m_prg->env.position = pos_garage_marker;
            if (m_prg->mech.isFinished()) {
                enter_goto_goal();
            }
            break;
    }
}

void rg::planner::enter_goto_goal() {
    m_master = upper_mode::travel_to_finish;
    m_goal = pos_finish;
    m_prg->mech.setCarriageMode(true);
    m_prg->env.writeSides();
    runDecision();
}

float rg::planner::relativeAngleTo(rg::rotation rot) {
    if (m_absoluteOdometry) {
        float toNearest = odometry::angle_to_nearest_theta(m_prg->odom.theta);
        float rotated = toNearest + (rot == rotation::left ? +90.0f : -90.0f);
        return rotated;
    } else {
        return rot == rotation::left ? +90.0f : -90.0f;
    }
}

void rg::planner::runDecision() {
    if (!field_map::isIndexValid(m_prg->env.position)) {
        std::cerr << "invalid position [" << "," << "]" << std::endl << std::flush;
        m_prg->env.position = {
                robofat::utility::limit(m_prg->env.position.x(), 0, field_map::width - 1),
                robofat::utility::limit(m_prg->env.position.y(), 0, field_map::height - 1)
        };
    }
    if (m_prg->env.position == m_goal) {
        std::cerr << "goal reached" << std::endl << std::flush;
        if (m_master == upper_mode::travel_to_garage) {
            enter_attach();
            return;
        } else {
            m_prg->mech.pushMove<cage_operation>(true);
            m_prg->mech.pushMove<cage_operation>(false);
        }
    }

    tile right = m_prg->map.get(m_prg->env.position.advance(rotate(m_prg->env.direction, rotation::right)));
    tile left = m_prg->map.get(m_prg->env.position.advance(rotate(m_prg->env.direction, rotation::left)));
    tile front = m_prg->map.get(m_prg->env.position.advance(m_prg->env.direction));

    if (m_prg->env.position == pos_start.advance(orient::north)) {
        goto toRight;
    }

    if (right == tile::free) {
        std::cerr << "[planner] going to right" << std::endl << std::flush;

        float angle = relativeAngleTo(rotation::right);
        if (m_prg->mdata.steerRegulated) {
            m_prg->mech.pushMove<steer_regulated>(m_steeringRadius, angle, true, m_prg->mdata.steerSpeed);
        } else {
            m_prg->mech.pushMove<steer_normal>(m_steeringRadius, angle, true, m_prg->mdata.steerSpeed);
        }
        m_prg->env.rotate(rotation::right);
        m_prg->env.advanceForward();
        enter_travel(travel_step::travel_to_steer_finish);
        return;
    }
    if (front == tile::unknown || front == tile::free) {
        std::cerr << "[planner] going forward" << std::endl << std::flush;
        if (m_wallFolowing) {
            m_prg->mech.pushMove<forward_wall_move>(m_cell2cellDistance);
        } else {
            m_prg->mech.pushMove<line_move>(m_cell2cellDistance);
        }
        m_prg->env.advanceForward();
        enter_travel(travel_step::travel_to_msr_point);
        return;
    }
    toRight:
    if (true) {
        std::cerr << "[planner] going to left" << std::endl << std::flush;
        float angle = relativeAngleTo(rotation::left);
        if (m_prg->mdata.steerRegulated) {
            m_prg->mech.pushMove<steer_regulated>(m_steeringRadius, angle, true, m_prg->mdata.steerSpeed);
        } else {
            m_prg->mech.pushMove<steer_normal>(m_steeringRadius, angle, true, m_prg->mdata.steerSpeed);
        }
        m_prg->env.rotate(rotation::left);
        m_prg->env.advanceForward();
        enter_travel(travel_step::travel_to_steer_finish);
        return;
    }
}

void rg::planner::planBackward(const pos &first, const pos &second, std::deque<pos> &rest) {
    pos third = rest.front();
    rest.pop_front();
    orient postDir = second.direction_to(third);
    relative_position rel = compare_directions(m_prg->env.direction, postDir);
    float angle;
    switch (rel) {
        case relative_position::new_same: // todo wtf dude
            break;
        case relative_position::new_to_left:
            std::cerr << "[planner] going backward to left" << std::endl << std::flush;

            angle = relativeAngleTo(rotation::left);
            if (m_prg->mdata.steerRegulated) {
                m_prg->mech.pushMove<steer_regulated>(m_steeringRadius, angle, false, m_prg->mdata.steerSpeed);
            } else {
                m_prg->mech.pushMove<steer_normal>(m_steeringRadius, angle, false, m_prg->mdata.steerSpeed);
            }
            m_prg->env.advanceBackward();
            m_prg->env.rotate(rotation::right);
            enter_travel(travel_step::travel_to_msr_point);
            break;
        case relative_position::new_to_right:
            std::cerr << "[planner] going backward to right" << std::endl << std::flush;

            angle = relativeAngleTo(rotation::right);
            if (m_prg->mdata.steerRegulated) {
                m_prg->mech.pushMove<steer_regulated>(m_steeringRadius, angle, false, m_prg->mdata.steerSpeed);
            } else {
                m_prg->mech.pushMove<steer_normal>(m_steeringRadius, angle, false, m_prg->mdata.steerSpeed);
            }
            m_prg->env.advanceBackward();
            m_prg->env.rotate(rotation::left);
            enter_travel(travel_step::travel_to_msr_point);
            break;
        case relative_position::new_opposite:
            std::cerr << "[planner] going backward" << std::endl << std::flush;
            m_prg->mech.pushMove<line_move>(m_cell2cellDistance, false, m_prg->mdata.bwdSpeed, true);
            m_prg->env.advanceBackward();
            enter_travel(travel_step::travel_to_msr_point);
            break;
    }
}


void rg::planner::enter_debug() {
    std::cerr << "[planner] start debug" << std::endl << std::flush;
    m_prg->odom.reset();
    m_master = upper_mode::debug;
    m_prg->mech.setCarriageMode(true);
    for (int i = 0; i < 4; i++) {
        m_prg->mech.pushMove<steer_regulated>(m_steeringRadius, relativeAngleTo(rotation::left), false,
                                              m_prg->mdata.steerSpeed);
        m_prg->mech.pushMove<line_move>(100.0f, true, m_prg->mdata.steerSpeed, true);
    }
}

void rg::planner::debug_loop() {}
