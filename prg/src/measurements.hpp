//
// Created by kuba on 19.3.19.
//

#ifndef ROBOGYMPL_MEASUREMENTS_HPP
#define ROBOGYMPL_MEASUREMENTS_HPP

#include <lego_color.hpp>
#include <lego_gyro.hpp>
#include <lego_touch.hpp>
#include <lego_ultrasonic.hpp>
#include "scheduler.hpp"

namespace rg {
    class measurements : public task {
    public:
        explicit measurements(program &prg);

        void update() override;

        int sonicDistance;
        int redReflection;
        int gyroAngle;
        bool touchPressed;
        int tachoLeft;
        int tachoRight;
        int speedLeft;
        int speedRight;
    };
}


#endif //ROBOGYMPL_MEASUREMENTS_HPP
