//
// Created by kuba on 25.3.19.
//

#include "rawreg.hpp"
#include "program.hpp"

rg::rawreg::rawreg(rg::program &prg)
        : task("raw motor regulator", prg),
          m_rqL(0.0f), m_rqR(0.0f), m_active(false),
          m_pidL(prg.cfg.get_float("motor reg kp"),
                 prg.cfg.get_float("motor reg ki"),
                 prg.cfg.get_float("motor reg kd"),
                 prg.cfg.get_float("motor reg imult"),
                 prg.cfg.get_float("motor reg imax"),
                 prg.cfg.get_float("motor reg dfilt now")),
          m_pidR(m_pidL.get_constants(), m_pidL.get_filter()) {}

int rg::rawreg::regulateMotor(float request, float current, float elapsed, robofat::pid &pid) {
    float err = request - current;

    bool overload = false;
    float pwr = pid.process(err, elapsed);
    if (pwr > 100.0f) {
        pwr = 100.0f;
        overload = true;
    }
    if (pwr < -100.0f) {
        pwr = -100.0f;
        overload = true;
    }
    pid.set_i_freeze(overload);
    return ftoi(pwr);
}

void rg::rawreg::update() {
    float elapsed = timeCalc();
    if (!m_active) {
        return;
    }

    int pwrL = regulateMotor(m_rqL, m_prg->msr.speedLeft, elapsed, m_pidL);
    int pwrR = regulateMotor(m_rqR, m_prg->msr.speedRight, elapsed, m_pidR);

    m_prg->dev.left.setDutyCycleSP(pwrL);
    m_prg->dev.right.setDutyCycleSP(pwrR);
}

void rg::rawreg::start() {
    if (!m_active) {
        setRequest(0.0f, 0.0f);
        m_prg->dev.left.setDutyCycleSP(0);
        m_prg->dev.right.setDutyCycleSP(0);
        m_prg->dev.left.setStopAction(stop_action::brake);
        m_prg->dev.right.setStopAction(stop_action::brake);
        m_prg->dev.left.doThe(motor_action::run_direct);
        m_prg->dev.right.doThe(motor_action::run_direct);
        m_pidL.get_state().reset();
        m_pidR.get_state().reset();
        m_active = true;
    }
}

void rg::rawreg::stop() {
    if (m_active) {
        m_prg->dev.left.doThe(motor_action::stop);
        m_prg->dev.right.doThe(motor_action::stop);
        m_prg->dev.left.setStopAction(stop_action::hold);
        m_prg->dev.right.setStopAction(stop_action::hold);
        m_active = false;
    }
}

void rg::rawreg::setRequest(float rqL, float rqR) {
    m_rqL = rqL;
    m_rqR = rqR;
}

float rg::rawreg::timeCalc() {
    timestamp now = {};
    timestamp diff = {};
    tsGet(now);
    tsDiff(m_last, now, diff);
    m_last = now;
    return tsMsec(diff);
}