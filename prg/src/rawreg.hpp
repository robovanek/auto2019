//
// Created by kuba on 25.3.19.
//

#ifndef ROBOGYMPL_RAWREG_HPP
#define ROBOGYMPL_RAWREG_HPP

#include <robofat.h>
#include "scheduler.hpp"

namespace rg {
    class program;

    class rawreg : public task {
    public:
        explicit rawreg(program &prg);

        void update() override;

        void setRequest(float rqL, float rqR);
        void start();
        void stop();

        static int regulateMotor(float request, float current, float elapsed, robofat::pid &pid);

    private:
        float timeCalc();

        timestamp m_last;
        float m_rqL;
        float m_rqR;
        bool m_active;
        robofat::pid m_pidL;
        robofat::pid m_pidR;
    };
}


#endif //ROBOGYMPL_RAWREG_HPP
