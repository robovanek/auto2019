//
// Created by kuba on 25.3.19.
//

#include <algorithm>
#include "angular_ramp.hpp"
#include <program.hpp>

rg::angular_ramp::angular_ramp(rg::program &prg)
        : m_prg(&prg),
          m_rampMin(prg.mdata.linearMoveDegrees(prg.cfg.get_float("ang ramp min mmps"))),
          m_rampUpAng(prg.cfg.get_float("ang rampup ang")),
          m_rampDownAng(prg.cfg.get_float("ang rampdown ang")),
          m_slowAng(prg.cfg.get_float("ang slow ang")) {}

float rg::angular_ramp::rampQuotient(float rotated, float remaining, float speed) {
    float a = rampUp(rotated);
    float b = speed;
    float c = rampDown(remaining);
    return std::max(std::min(std::min(a, b), c), m_rampMin) / speed;
}

float rg::angular_ramp::rampUp(float rotated) {
    return rotated * (m_prg->mech.getMaxSpeed() / m_rampUpAng);
}

float rg::angular_ramp::rampDown(float remaining) {
    return (remaining - m_slowAng) * (m_prg->mech.getMaxSpeed() / m_rampDownAng);
}
