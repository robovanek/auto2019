//
// Created by kuba on 25.3.19.
//

#ifndef ROBOGYMPL_LINEAR_RAMP_HPP
#define ROBOGYMPL_LINEAR_RAMP_HPP

namespace rg {

    class program;
    class linear_ramp {
    public:
        explicit linear_ramp(program &prg);

        float rampQuotient(float travelled, float remaining, float speed);

        float travelled(int l0, int r0);
        float remaining(int lSP, int rSP);
    private:

        float rampUp(float travelled);
        float rampDown(float remaining);

        program *m_prg;
        float m_slowDegs;
        float m_rampUpDegs;
        float m_rampDownDegs;
        float m_rampMin;
    };
}


#endif //ROBOGYMPL_LINEAR_RAMP_HPP
