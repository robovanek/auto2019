//
// Created by kuba on 19.3.19.
//

#include <program.hpp>

rg::device_ptr findDrv(ev3udev::Registry &reg, const std::string &driver) {
    if (auto &&dev = reg.findForDriver(driver)) {
        return dev;
    } else {
        std::ostringstream ostr;
        ostr << "Cannot find one of the devices: " << driver;
        throw std::runtime_error(ostr.str());
    }
}

rg::device_ptr findPort(ev3udev::Registry &reg, const std::string &port) {
    if (auto &&dev = reg.findForAddress(port)) {
        return dev;
    } else {
        std::ostringstream ostr;
        ostr << "Cannot find one of the devices: @" << port;
        throw std::runtime_error(ostr.str());
    }
}


rg::device_store::device_store(program &prg)
        : sonic(findDrv(prg.reg, lego_ultrasonic::driver)),
          color(findDrv(prg.reg, lego_color::driver)),
          touch(findDrv(prg.reg, lego_touch::driver)),
          gyro(findDrv(prg.reg, lego_gyro::driver)),
          cage(findDrv(prg.reg, motor::medium_driver)),
          left(findPort(prg.reg, prg.cfg.get_port("left motor port"))),
          right(findPort(prg.reg, prg.cfg.get_port("right motor port"))) {
    sonic.setMode(ultrasonic_mode::distance_cm);
    color.setMode(color_mode::COL_REFLECT);
    touch.setMode(touch_mode::TOUCH);
    gyro.setMode(gyro_mode::GYRO_ANG);
    cage.doThe(motor_action::reset);
    left.doThe(motor_action::reset);
    right.doThe(motor_action::reset);
}
