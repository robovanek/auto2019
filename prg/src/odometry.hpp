//
// Created by kuba on 19.3.19.
//

#ifndef ROBOGYMPL_ODOMETRY_HPP
#define ROBOGYMPL_ODOMETRY_HPP

#include <scheduler.hpp>
#include <settings.h>
#include "field_map.hpp"

namespace rg {
    class program;

    class odometry : public task {
    public:
        explicit odometry(program &prg);

        void update() override;

        float x = pos_start.x() * 280.0f + 140.0f;
        float y = pos_start.y() * 280.0f + 140.0f;
        float velocity = 0.0f;
        float theta = 0.0f;
        float omega = 0.0f;

        static float theta_from_orient(orient where);

        static orient theta_to_orient(float theta);

        static std::tuple<float, float> coords_from_pos(const pos &p);

        static pos coords_to_pos(float x, float y);

        static pos coords_to_pos(const std::tuple<float, float> &c) {
            return coords_to_pos(std::get<0>(c), std::get<1>(c));
        }

        static float angle_to_nearest_theta(float angle);

        static float nearest_theta(float theta);

        static std::tuple<float, float> nearest_pos(const std::tuple<float, float> &coords);

        std::tuple<float, float> asTuple() const;

        void fromTuple(const std::tuple<float, float> &tuple);

        void reset();

    private:
        int m_oldL = 0;
        int m_oldR = 0;
        int m_oldGyro = 0;
        bool m_hasOld = false;
        float m_wheelRadius;
        float m_trackRadius;
        float m_gyroAngleTrust;
        bool m_gyrodometry;
        float m_gyrodometry_threshold;
    };
}


#endif //ROBOGYMPL_ODOMETRY_HPP
