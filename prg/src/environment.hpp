//
// Created by kuba on 21.3.19.
//

#ifndef ROBOGYMPL_ENVIRONMENT_PROCESSOR_HPP
#define ROBOGYMPL_ENVIRONMENT_PROCESSOR_HPP

#include "scheduler.hpp"
#include "field_map.hpp"
#include <settings.h>

namespace rg {
    class environment : public task {
    public:
        explicit environment(program &prg);

        void update() override;

        void writeSides();

        void writeWallHit();

        void advanceForward();

        void advanceBackward();

        void rotate(rotation rot);

        void print();

        pos position = pos_start;
        orient direction = orient::north;
    private:
        int m_sonicSideSpace;
        int m_colorThreshold;
        int m_sonicMaxFree;
        bool m_colorOnRight;
        int m_counter = 0;
    };
}


#endif //ROBOGYMPL_ENVIRONMENT_PROCESSOR_HPP
