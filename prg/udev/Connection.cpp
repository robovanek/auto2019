#include <utility>

#include <libudev.h>
#include <unistd.h>
#include <cstring>

#include "Connection.h"
#include "Device.h"
#include "Registry.hpp"
#include "program.hpp"

ev3udev::Connection::Connection(rg::program &prg, udev_handle udevInstance, Registry &registry)
        : Connection(prg, std::move(udevInstance), registry, {"lego-sensor", "tacho-motor"}) {}

ev3udev::Connection::Connection(rg::program &prg, udev_handle udevInstance, Registry &registry,
                                std::vector<std::string> match_subsystems)
        : rg::task("udev monitor", prg),
          subsystems(std::move(match_subsystems)),
          udev(std::move(udevInstance)),
          mon(make_udevmon(udev)),
          registry(&registry) {

    if (mon == nullptr) {
        raise_errno("make_udevmon", "ev3udev::Connection::Connection");
    }

    for (const std::string &subsys : subsystems) {
        udev_monitor_filter_add_match_subsystem_devtype(mon.get(), subsys.c_str(), nullptr);
    }
    udev_monitor_filter_update(mon.get());
    udev_monitor_enable_receiving(mon.get());
    enumerate();
}

void ev3udev::Connection::update() {
    while (udevdev_handle dev = udevdev_handle(udev_monitor_receive_device(mon.get()), udev_device_unref)) {
        std::string action = udev_device_get_action(dev.get());
        std::string syspath = udev_device_get_syspath(dev.get());
        if (action == "add") {
            registry->deviceAdded(syspath);
        } else if (action == "change") {
            registry->deviceChanged(syspath);
        } else if (action == "remove") {
            registry->deviceRemoved(syspath);
        }
    }
}

void ev3udev::Connection::enumerate() {
    udevenum_handle list = make_udevenum(udev);
    for (const std::string &subsys : subsystems) {
        udev_enumerate_add_match_subsystem(list.get(), subsys.c_str());
    }
    udev_enumerate_add_match_is_initialized(list.get());
    udev_enumerate_scan_devices(list.get());
    udev_list_entry *found = udev_enumerate_get_list_entry(list.get());
    udev_list_entry *it;
    udev_list_entry_foreach(it, found) {
        std::string name = udev_list_entry_get_name(it);
        registry->deviceAdded(name);
    }
}