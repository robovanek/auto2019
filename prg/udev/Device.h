#ifndef UDEVDEVICE_INTERNAL
#define UDEVDEVICE_INTERNAL

#include "Device.h"
#include "Common.h"
#include "Attribute.h"

namespace ev3udev {

    class Device {
    public:
        explicit Device(udev_handle udev, const std::string &syspath);

        ~Device();

        std::string get_action();

        std::string get_sys_path();

        std::string get_sys_name();

        std::string get_subsystem();

        std::string get_devtype();

        std::string get_lego_driver();

        std::string get_lego_address();

        bool hasAttr(const std::string &name);

        std::shared_ptr<Attribute> attr(const std::string &name);

        bool present();

        void handleUdevChange();

        void handleUdevRemove();

    private:
        void enumerateAttrs();

        void iterateDir(const std::string &base, const std::string &path);

        void appendAttr(const std::string &base, const std::string &path);

        std::unordered_map<std::string, std::shared_ptr<Attribute>> attrs;
        std::string syspath;
        udev_handle udev;
        udevdev_handle dev;
        bool is_present;
    };

}


#endif // UDEVDEVICE_INTERNAL
