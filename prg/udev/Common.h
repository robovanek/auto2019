#ifndef UDEVINSTANCE_H
#define UDEVINSTANCE_H

#include <libudev.h>
#include <memory>
#include <iostream>
#include <sstream>

namespace ev3udev {
    typedef std::shared_ptr<udev> udev_handle;
    typedef std::shared_ptr<udev_monitor> udevmon_handle;
    typedef std::shared_ptr<udev_device> udevdev_handle;
    typedef std::shared_ptr<udev_enumerate> udevenum_handle;

    inline udev_handle make_udev() {
        return udev_handle(udev_new(), udev_unref);
    }

    inline udevmon_handle make_udevmon(const udev_handle &udev) {
        return udevmon_handle(udev_monitor_new_from_netlink(udev.get(), "udev"), udev_monitor_unref);
    }

    inline udevdev_handle make_udevdev_from_syspath(const udev_handle &udev, const std::string &syspath) {
        return udevdev_handle(udev_device_new_from_syspath(udev.get(), syspath.c_str()), udev_device_unref);
    }

    inline udevenum_handle make_udevenum(const udev_handle &udev) {
        return udevenum_handle(udev_enumerate_new(udev.get()), udev_enumerate_unref);
    }

    void raise_errno(std::string &&operation, const std::string &path);

    void log_errno(std::string &&operation, const std::string &path);
}


#endif // UDEVINSTANCE_H
