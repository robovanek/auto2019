#ifndef UDEVSYSATTRIBUTE_INTERNAL_H
#define UDEVSYSATTRIBUTE_INTERNAL_H

#include <vector>
#include <unordered_map>

#include "Attribute.h"

#define INITIAL_ATTRIBUTE_CAPACITY 16

namespace ev3udev {

    enum class access_mode {
        unknown,
        no_access,
        read_only,
        write_only,
        read_write
    };

    class Attribute {
    public:
        explicit Attribute(const std::string &syspath);

        ~Attribute();

        void open();

        void close();

        const char *read();

        std::string readString();

        std::vector<std::string> readStringList();

        int readInt();

        float readFloat();

        void write(const char *buf);

        void writeString(const std::string &str);

        void writeInt(int value);

        void writeFloat(float value);

        const std::string &getSyspath() const;

        access_mode getAccess();

        void handleRemove();

        int lastReadLength() const {
            return lastReadLen;
        }

    private:
        int fd = -1;
        int lastReadLen = 0;
        access_mode type = access_mode::unknown;
        std::string path;
        std::vector<char> rdbuf;
        bool present;
    };

}


#endif // UDEVSYSATTRIBUTE_INTERNAL_H
