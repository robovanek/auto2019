//
// Created by kuba on 18.3.19.
//

#include <cstring>
#include "Common.h"

std::string get_errno_message(std::string &&operation, const std::string &path) {
    int err = errno;
    std::ostringstream str;
    str << "IO error: Cannot do " << operation << "() at '" << path << "': errno=" << err << " [" << strerror(err)
        << "].";
    return str.str();
}

void ev3udev::raise_errno(std::string &&operation, const std::string &path) {
    throw std::runtime_error(get_errno_message(std::move(operation), path));
}

void ev3udev::log_errno(std::string &&operation, const std::string &path) {
    std::cerr << get_errno_message(std::move(operation), path) << std::endl;
}
