#include "Attribute.h"
#include "Common.h"

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <locale>
#include <cstring>

////////////////
// ATTRIBUTES //
////////////////

ev3udev::Attribute::Attribute(const std::string &syspath)
        : path(syspath), rdbuf(INITIAL_ATTRIBUTE_CAPACITY), present(true) {}

ev3udev::Attribute::~Attribute() {
    close();
}

void ev3udev::Attribute::open() {
    if (!present) {
        throw std::runtime_error("Attribute at " + path + " isn't present anymore.");
    }
    if (fd == -1) {
        int opts = 0;
        switch (getAccess()) {
            case access_mode::unknown:
            case access_mode::no_access:
                opts |= 0;
                break;
            case access_mode::read_only:
                opts |= O_RDONLY;
                break;
            case access_mode::write_only:
                opts |= O_WRONLY;
                break;
            case access_mode::read_write:
                opts |= O_RDWR;
                break;
        }
        fd = ::open(path.c_str(), opts);
        if (fd < 0) {
            raise_errno("open", path);
        }
    }
}

void ev3udev::Attribute::close() {
    if (fd != -1) {
        if (::close(fd) < 0) {
            log_errno("close", path);
        }
        fd = -1;
    }
}

const char *ev3udev::Attribute::read() {
    open();
    if (type != access_mode::read_only && type != access_mode::read_write)
        throw std::runtime_error("Trying to read non-readable attribute " + this->path);
    ssize_t bytesRead = 0;
    while (true) {
        if (::lseek(fd, 0, SEEK_SET) < 0) {
            raise_errno("lseek", path);
        }
        bytesRead = ::read(fd, rdbuf.data(), rdbuf.capacity());
        if (bytesRead < 0) {
            raise_errno("read", path);
        }
        if (bytesRead == rdbuf.size()) {
            rdbuf.clear();
            rdbuf.resize(rdbuf.capacity() * 2);
        } else {
            break;
        }
    }
    lastReadLen = static_cast<int>(bytesRead);
    rdbuf[bytesRead] = '\0';
    return rdbuf.data();
}

int ev3udev::Attribute::readInt() {
    int result = 0;
    std::string str(read());
    std::istringstream istr(str);
    istr.imbue(std::locale::classic());
    istr >> result;
    return result;
}

float ev3udev::Attribute::readFloat() {
    float result = 0;
    std::string str(read());
    std::istringstream istr(str);
    istr.imbue(std::locale::classic());
    istr >> result;
    return result;
}

void ev3udev::Attribute::write(const char *buf) {
    open();
    if (type != access_mode::write_only && type != access_mode::read_write)
        throw std::runtime_error("Trying to write non-writable attribute " + this->path);

    if (::lseek(fd, 0, SEEK_SET) < 0) {
        raise_errno("lseek", path);
    }
    if (::write(fd, buf, strlen(buf)) < 0) {
        raise_errno("write", path);
    }
    if (::fsync(fd) < 0) {
        raise_errno("write", path);
    }
}

const std::string &ev3udev::Attribute::getSyspath() const {
    return path;
}

ev3udev::access_mode ev3udev::Attribute::getAccess() {
    if (type == access_mode::unknown) {
        // this intentionally checks numerical permissions, because sysfs by default allows only what is supported
        struct stat info = {};
        int result = stat(path.c_str(), &info);
        if (result < 0) {
            raise_errno("stat", path);
        }

        if (info.st_mode & S_IRUSR) {
            if (info.st_mode & S_IWUSR) {
                type = access_mode::read_write;
            } else {
                type = access_mode::read_only;
            }
        } else {
            if (info.st_mode & S_IWUSR) {
                type = access_mode::write_only;
            } else {
                type = access_mode::no_access;
            }
        }
    }
    return type;
}

std::string ev3udev::Attribute::readString() {
    const char *ptr = read();
    const char *end = ptr;
    while (*end != '\0' && *end != '\n')
        ++end;
    return std::string(read(), end - ptr);
}

void ev3udev::Attribute::writeString(const std::string &str) {
    write(str.c_str());
}

void ev3udev::Attribute::writeInt(int value) {
    std::ostringstream ostr;
    ostr.imbue(std::locale::classic());
    ostr << value;
    writeString(ostr.str());
}

void ev3udev::Attribute::writeFloat(float value) {
    std::ostringstream ostr;
    ostr.imbue(std::locale::classic());
    ostr << value;
    writeString(ostr.str());
}

std::vector<std::string> ev3udev::Attribute::readStringList() {
    std::vector<std::string> result;

    const char *pch = read();
    const char *begin = pch;
    for (; *pch != '\0' && *pch != '\n'; ++pch) {
        if (*pch == ' ') {
            result.emplace_back(begin, pch - begin);
            begin = pch + 1;
        }
    }
    result.emplace_back(begin, pch - begin);
    return result;
}

void ev3udev::Attribute::handleRemove() {
    present = false;
    close();
}