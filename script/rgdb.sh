#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

gdb-multiarch -ex 'set sysroot /home/kuba/sysroot' \
              -ex 'set substitute-path /home/compiler/program /home/kuba/lego/robo-repo/auto2019/prg' \
              -ex 'target remote 10.0.0.41:6666' \
              /home/kuba/lego/robo-repo/auto2019/prg/build/robogympl
