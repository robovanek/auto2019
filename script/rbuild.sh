#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

docker run --rm -it -v "$DIR:/home/compiler/program" linuxtardis:auto2019 /home/compiler/build.sh
