#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

ssh -t autobot brickrun -- gdbserver localhost:6666 /home/robot/a_program
