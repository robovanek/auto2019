#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

scp "$DIR/build/robogympl" "autobot:/home/robot/a_program"
ssh autobot sudo setcap cap_sys_nice+ep /home/robot/a_program
