#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

if [[ "$1" = "release" ]]; then
    BUILD_TYPE="Release"
elif [[ "$1" = "debug" ]]; then
    BUILD_TYPE="Debug"
else
    BUILD_TYPE="RelWithDebInfo"
fi

docker run --rm -it -v "$DIR:/home/compiler/program" -e BUILD_TYPE=$BUILD_TYPE linuxtardis:auto2019 /home/compiler/cmake.sh
